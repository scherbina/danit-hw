const hamburger = document.querySelector('.hamburger');
const navbarMenu = document.querySelector('.navbarmenu');

hamburger.addEventListener('click', function (event) {
    hamburger.classList.toggle('active');
    hamburger.classList.contains('active') ? navbarMenu.style.display = 'block' : navbarMenu.style.display = 'none';
    const coords = hamburger.getBoundingClientRect();
    navbarMenu.style.top = coords.bottom + 'px';
});

window.addEventListener('resize', render);
render();

function render () {
    const w = window.innerWidth;
    
    if (w >= 768) {
        navbarMenu.style.display = 'flex';
        navbarMenu.style.removeProperty("display");
        navbarMenu.style.removeProperty("top");
        hamburger.classList.remove('active');
        document.querySelectorAll('.contacts-item').forEach((el) => el.style.marginLeft = ``);
    } else {
        if (w > 320) {
            const borders = 10;
            const marginIcon = 20;
            const coordsContactsIcon = document.querySelector('.contacts-icon').getBoundingClientRect();
            const coordsContactsInfoWrapper = document.querySelector('.contacts-info-wrapper').getBoundingClientRect();
            const marginLeft = (w - borders - (coordsContactsIcon.width + marginIcon + coordsContactsInfoWrapper.width)) / 2;
            document.querySelectorAll('.contacts-item').forEach((el) => el.style.marginLeft = `${marginLeft}px`);
        } else return
    }
};