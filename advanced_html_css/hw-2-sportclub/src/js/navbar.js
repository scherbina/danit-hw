const hamburger = document.querySelector('.hamburger');
const navbarNav = document.querySelector('.navbar__nav');

hamburger.addEventListener('click', function (event) {
    hamburger.classList.toggle('hamburger--active');
    renderNavbarNav()
});

window.addEventListener('resize', renderNavbarNav);

function renderNavbarNav () {
    const w = window.innerWidth;
    if (w <= 768) {
        hamburger.classList.contains('hamburger--active') ? navbarNav.style.display = 'block' : navbarNav.style.display = 'none';
        const coordsHamburger = hamburger.getBoundingClientRect();
        const coordsNavbarNav = navbarNav.getBoundingClientRect();
        navbarNav.style.top = coordsHamburger.bottom + 'px';
        navbarNav.style.left = (coordsHamburger.right - coordsNavbarNav.width) + 'px';
    } else {
        navbarNav.style.display = '';      
    }
};