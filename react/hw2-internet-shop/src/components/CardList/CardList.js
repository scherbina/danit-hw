import React, { Component } from 'react';
import './CardList.scss';
import Card from '../Card/Card';

class CardList extends Component {
    render() {
        const {data, onAddToCart} = this.props;
        const cards = data.map(card => (<Card key={card.id} card={card} onAddToCart={onAddToCart}/>))

        return (
            <>
               <h4 className='cardlist-title'>Spark-plugs</h4>
               <ul className='cardlist'>
                    {cards}
               </ul>
            </>
        );
    }
}

export default CardList;
