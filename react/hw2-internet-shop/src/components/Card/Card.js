import React, { Component } from 'react';
import Button from '../Button/Button';
import './Card.scss';

class Card extends Component {
    
    state = {
        isFavourite: false
    }
    
    componentDidMount() {
        const {id} = this.props.card;
        const favourites = localStorage.hasOwnProperty('favourites') ? JSON.parse(localStorage.getItem('favourites')) : [];
        this.setState({isFavourite: favourites.includes(id)})
    }

    onFavouriteIconClick = () => {
        const {id} = this.props.card;
        const oldFavourites = localStorage.hasOwnProperty('favourites') ? JSON.parse(localStorage.getItem('favourites')) : [];
        const newFavourites = oldFavourites.includes(id) ? oldFavourites.filter(itemid => itemid !== id) : [...oldFavourites, id]

        localStorage.setItem('favourites', JSON.stringify(newFavourites))
        this.setState({isFavourite: !this.state.isFavourite})
    }

    render() {
        const {id, name, image, vendorcode, color, price} = this.props.card;
        const {onAddToCart} = this.props;
        const {isFavourite} = this.state;
        
        const favouriteIconSrc = (isFavourite ? '/star-favourite.svg' : '/star-unfavourite.svg')

        return (
            <div className='card'>
                <img className='card-image' src={image} alt={`${name} ${vendorcode}`}/>
                <div className='card-title'>
                    <span className='card-name'>{name}</span>
                    <span className='card-favourite' onClick={this.onFavouriteIconClick}>
                        <img src={favouriteIconSrc} alt='Favourite icon'/>
                    </span>
                </div>
                <p className='card-vendorcode'>{vendorcode}</p>
                <p className='card-color'>Color: {color}</p>
                <p className='card-price'>{price} грн </p>
                <Button backgroundColor='#ccc' text='Add to CART...' onClick={onAddToCart.bind(null, id, name, vendorcode)}/>
            </div>
        );
    }
}

export default Card;
