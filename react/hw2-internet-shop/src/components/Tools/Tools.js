import React from 'react';

function AddLineBreaks(string) {
    return (
        string
            .split('\n')
            .map((text, index) => 
                (
                <React.Fragment key={`${text}-${index}`}>
                    {text}
                    <br />
                </React.Fragment>
                )
            )
    )
}

export default AddLineBreaks;
