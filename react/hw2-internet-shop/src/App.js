import React, { Component } from 'react';
import './App.scss';
import axios from 'axios';
import CardList from './components/CardList/CardList';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';

class App extends Component {
  state = {
    data: null,
    addToCart: {
      isAddToCart: false,
      id: null
    }
    
  }

  componentDidMount() {
    axios("/goods.json")
      .then(response => {
        if (response.status === 200) {
          this.setState({data: response.data});
        }
      })
  }

  getCartFromStorage = () => {
    if (localStorage.hasOwnProperty('cart')) 
        return JSON.parse(localStorage.getItem('cart'));
        else return [];
  }
  
  openWarningAddToCart = (id, name, vendorcode) => {
    this.setState({
      addToCart: {
        isAddToCart: true,
        id, 
        name, 
        vendorcode
      }
    });
  }

  closeWarningAddToCart = () => {
    this.setState({
        addToCart: {
          isAddToCart: false,
          id: null
        }
    });
  }

  addToCart = () => {
    const {id} = this.state.addToCart;
    console.log('Added to cart id:', id);

    const oldCart = localStorage.hasOwnProperty('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    console.log('Old cart: ', oldCart);
    
    let isInCart = false;
    const newCart = oldCart.map(itemInCart => {
      if (itemInCart.id === id) {
        itemInCart.count++;
        isInCart = true
      }
      return {id: itemInCart.id, count: itemInCart.count}
    });

    if (!isInCart) newCart.push({id, count: 1});
    console.log('New cart: ', newCart);
    localStorage.setItem('cart', JSON.stringify(newCart))

    this.closeWarningAddToCart();
  }

  render() {
    
    const {data} = this.state;
    const {isAddToCart, name, vendorcode} = this.state.addToCart;

    const actionsAddToCart = [
      <Button key='0' backgroundColor='#aaa' text='Yes' onClick={this.addToCart}/>,
      <Button key='1' backgroundColor='#aaa' text='Cancel' onClick={this.closeWarningAddToCart}/>
    ]; 
    
    return (
      <div className="App">
        {data && <CardList data={data} onAddToCart={this.openWarningAddToCart}/>}
        {isAddToCart && 
          <Modal 
            header='Do you realy want to add to cart?' 
            closeButton={true} 
            text={`Name: ${name}\nVendor code: ${vendorcode}`}
            actions={actionsAddToCart} 
            onCloseClick={this.closeWarningAddToCart}
          />
        }
      </div>
    );
  }
}

export default App;
