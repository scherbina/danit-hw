import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Favorites from '../pages/Favorites/Favorites';
import Cart from '../pages/Cart/Cart';
import CardList from '../components/CardList/CardList';

const AppRoutes = (props) => {
    const {data, cart, favorites, onUpdateCart, onFavoriteToogle} = props;

    return (
      <>
        {/* <Route path='/' component={Navbar} /> */}

        <Switch>
          {data && 
            <>
            <Route exact path='/' render={() => <CardList data={data} cart={cart} favorites={favorites} onUpdateCart={onUpdateCart} onFavoriteToogle={onFavoriteToogle}/>} />
            <Route exact path='/favorites' render={() => <Favorites data={data} cart={cart} favorites={favorites} onUpdateCart={onUpdateCart} onFavoriteToogle={onFavoriteToogle}/>} />
            <Route exact path='/cart' render={() => <Cart data={data} cart={cart} favorites={favorites} onUpdateCart={onUpdateCart} onFavoriteToogle={onFavoriteToogle}/>} />
            </>
          }
        </Switch>
      </>
    )
}

export default AppRoutes