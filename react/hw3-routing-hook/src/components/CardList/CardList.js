import React from 'react';
import './CardList.scss';
import Card from '../Card/Card';

const CardList = (props) => {
        const {data, cart, favorites, onUpdateCart, onFavoriteToogle} = props;

        const cards = data.map(card => {
            const countInCart = !!cart && cart.find(el => el.id === card.id);
            return <Card key={card.id} card={card} countInCart={!!countInCart ? countInCart.count : 0} isFavorite={!!favorites && favorites.includes(card.id)} onUpdateCart={onUpdateCart} onFavoriteToogle={onFavoriteToogle}/>
        })

        return (
            <>
               <ul className='cardlist'>
                    {cards}
               </ul>
            </>
        );
}

export default CardList;
