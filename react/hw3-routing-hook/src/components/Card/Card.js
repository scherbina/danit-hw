import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Button from '../Button/Button';
import Icon from '../Icon/Icon';
import './Card.scss';

const Card = (props) => {
        const {countInCart, isFavorite, onUpdateCart, onFavoriteToogle} = props;
        const {id, name, image, vendorcode, color, price} = props.card;

        return (
            <div className='card'>
                <img className='card-image' src={image} alt={`${name} ${vendorcode}`}/>
                <div className='card-title'>
                    <span className='card-name'>{name}</span>
                    <Icon type='star' filled={isFavorite} className='card-favourite' onClick={onFavoriteToogle.bind(null, id)}/>
                </div>
                <p className='card-vendorcode'>{vendorcode}</p>
                <p className='card-color'>Color: {color}</p>
                <p className='card-price'>{price} UAH </p>
                <p className='card-countInCart'>In CART: {countInCart} </p>
                <Switch>
                    <Route exact path='/cart' render={() => <Button backgroundColor='#ccc' text='Delete 1 from CART...' onClick={onUpdateCart.bind(null, 'DELETE', id, name, vendorcode)}/>} />
                    <Route path='*' render={() => <Button backgroundColor='#ccc' text='Add 1 to CART...' onClick={onUpdateCart.bind(null, 'ADD', id, name, vendorcode)}/>} />
                </Switch>
                <Route exact path='/cart' render={() => <button className='card-btn-delete' onClick={onUpdateCart.bind(null, 'DELETE ALL', id, name, vendorcode)}>X</button>} />
            </div>
        );
}

export default Card;
