import React from 'react'
import CardList from '../../components/CardList/CardList';

const Cart = (props) => {
  const {data, cart, favorites, onUpdateCart, onFavoriteToogle} = props;
    
  let dataCart;
  if (!!cart) {
    const idInCart = cart.map(cart => cart.id);
    dataCart = data.filter( ({id}) => idInCart.includes(id));  
  } else dataCart = [];

  return (
    <>
      <h4 className='cardlist-title'>Cart</h4>
      <CardList data={dataCart} cart={cart} favorites={favorites} onUpdateCart={onUpdateCart} onFavoriteToogle={onFavoriteToogle}/>
    </>
  )
}

export default Cart