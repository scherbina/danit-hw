import React from 'react'
import CardList from '../../components/CardList/CardList';

const Favorites = (props) => {
    const {data, cart, favorites, onUpdateCart, onFavoriteToogle} = props;
    
    const dataFavorite = !!favorites ? data.filter( ({id}) => favorites.includes(id)) : [];

    return (
      <>
        <h4 className='cardlist-title'>Favorites</h4>
        <CardList data={dataFavorite} cart={cart} favorites={favorites} onUpdateCart={onUpdateCart} onFavoriteToogle={onFavoriteToogle}/>
      </>
    )
}

export default Favorites