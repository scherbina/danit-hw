import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';
import Navbar from './components/Navbar/Navbar';
import AppRoutes from './routes/AppRoutes';
import './App.scss';


const App = () => {
    //Все товары
    const [data, setData] = useState(null);
    //Избранное
    const [favorites, setFavorites] = useState(null);
    //Корзина
    const [cart, setCart] = useState(null);

    //Товар, который обновляет корзину
    const [{isItemUpdateCart, actionUpdateCart, idItemUpdateCart, nameItemUpdateCart, vendorcodeItemUpdateCart}, setItemUpdateCart] = useState(
      {
      isItemUpdateCart: false, 
      actionUpdateCart: null,
      idItemUpdateCart: null, 
      nameItemUpdateCart: null, 
      vendorcodeItemUpdateCart: null
      }
    );

    //Загрузка данных
    useEffect(() => {
      axios("/goods.json")
        .then(response => {
          if (response.status === 200) {
            setData(response.data);
            setFavorites(localStorage.hasOwnProperty('favourites') ? JSON.parse(localStorage.getItem('favourites')) : []);
            setCart(localStorage.hasOwnProperty('cart') ? JSON.parse(localStorage.getItem('cart')) : []);
          }
        })
      
    }, [])

    //Открытие модального окна предупреждения обновления корзины
    const openWarningUpdateCart = (actionUpdateCart, idItemUpdateCart, nameItemUpdateCart, vendorcodeItemUpdateCart) => {
      setItemUpdateCart(
        {
          isItemUpdateCart: true,
          actionUpdateCart,
          idItemUpdateCart, 
          nameItemUpdateCart, 
          vendorcodeItemUpdateCart
        }
      );
    }
    
    //Закрытие модального окна предупреждения обновления корзины
    const closeWarningUpdateCart = () => {
      setItemUpdateCart({
        isItemUpdateCart: false, 
        actionUpdateCart: null,
        idItemUpdateCart: null, 
        nameItemUpdateCart: null, 
        vendorcodeItemUpdateCart: null
        });
    }

    //Изменение корзины
    const updateCart = (action) => {

      let isInCart = false;
      let newCart = cart.map(itemInCart => {
        let newCount = itemInCart.count;
        if (itemInCart.id === idItemUpdateCart) {
          newCount = newCount + action;
          isInCart = true
        }
        return {id: itemInCart.id, count: newCount}
      });
    
      if ((action > 0) && !isInCart ) newCart.push({id: idItemUpdateCart, count: action});
      if (action < 0) newCart = newCart.filter(itemInCart => itemInCart.count > 0);

      localStorage.setItem('cart', JSON.stringify(newCart))
      setCart(newCart)

      closeWarningUpdateCart();
    }

    //Переключение признака Favorite
    const onFavoriteToogle = (idItemFavoriteToogle) => {
      const newFavourites = favorites.includes(idItemFavoriteToogle) ? favorites.filter(itemid => itemid !== idItemFavoriteToogle) : [...favorites, idItemFavoriteToogle]
      localStorage.setItem('favourites', JSON.stringify(newFavourites))
      setFavorites(newFavourites)
    }

    //Кнопки в модалке updateCart
    const actionsUpdateCart = [
      (actionUpdateCart === 'ADD') && <Button key='0' backgroundColor='#aaa' text='Add to Cart' onClick={updateCart.bind(null, 1)}/>,
      (actionUpdateCart === 'DELETE') && <Button key='0' backgroundColor='#aaa' text='Delete 1 piece from Cart' onClick={updateCart.bind(null, -1)}/>,
      <Button key='1' backgroundColor='#aaa' text='Cancel' onClick={closeWarningUpdateCart}/>,
      (actionUpdateCart === 'DELETE ALL') && <Button key='9' backgroundColor='#aaa' text='Delete all pieces from Cart' onClick={updateCart.bind(null, -999999999)}/>,
    ]; 
    
    return (
      <div className="App">
        <Navbar />
        <AppRoutes data={data} cart={cart} favorites={favorites} onUpdateCart={openWarningUpdateCart} onFavoriteToogle={onFavoriteToogle} />
        {isItemUpdateCart &&
          <Modal 
            header='Do you realy want to change cart?' 
            closeButton={true} 
            text={`Action: ${actionUpdateCart}\nName: ${nameItemUpdateCart}\nVendor code: ${vendorcodeItemUpdateCart}`}
            actions={actionsUpdateCart} 
            onCloseClick={closeWarningUpdateCart}
          />
        }
      </div>
    );

}

export default App;
