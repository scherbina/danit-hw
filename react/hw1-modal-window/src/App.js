import React, { Component } from 'react';
import Button from './components/Button/Button'
import './App.scss';
import Modal from './components/Modal/Modal';

class App extends Component {
  state = {
    showOpenFileWarning: false,
    showDeleteFileWarning: false,
  }

  showOpenFileWarning = () => {
    this.setState({showOpenFileWarning: true});
  }

  showDeleteWarning = () => {
    this.setState({showDeleteFileWarning: true});
  }

  closeDeleteFileWarning = () => {
    this.setState({showDeleteFileWarning: false});
  }

  closeOpenFileWarning = () => {
    this.setState({showOpenFileWarning: false});
  }

  onConfirmOpen = () => {
    console.log('File opened!!!');
    this.closeOpenFileWarning();
  }

  onConfirmDelete = () => {
    console.log('File deleted!!!');
    this.closeDeleteFileWarning();
  }

  render() {
    const {showOpenFileWarning, showDeleteFileWarning} = this.state;
    const actionsOpen = [
      <Button key='0' backgroundColor='#b3382c' text='Yes' onClick={this.onConfirmOpen}/>,
      <Button key='1' backgroundColor='#b3382c' text='Cancel' onClick={this.closeOpenFileWarning}/>
    ];

    const actionsDelete = [
      <Button key='0' backgroundColor='#b3382c' text='Yes' onClick={this.onConfirmDelete}/>,
      <Button key='1' backgroundColor='#b3382c' text='Cancel' onClick={this.closeDeleteFileWarning}/>
    ]

    return (
      <div className="App">
        <Button backgroundColor='green' text='Open' onClick={this.showOpenFileWarning}/>
        <Button backgroundColor='red' text='Delete' onClick={this.showDeleteWarning}/>
        {showOpenFileWarning && 
          <Modal 
            header='Do you want to open this file?' 
            closeButton={false} 
            text={"This file may be dangerous.\nAre you sure you want to open it?"}
            actions={actionsOpen} 
            onCloseClick={this.closeOpenFileWarning}
          />}
        {showDeleteFileWarning && 
          <Modal 
            header='Do you want to delete this file?' 
            closeButton={true} 
            text={"Once you delete this file, it won't be possible to undo this action.\nAre you sure you want to delete it?"}
            actions={actionsDelete} 
            onCloseClick={this.closeDeleteFileWarning}
          />
        }
      </div>
    );
  }
}

export default App;
