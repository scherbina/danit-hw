import React, { PureComponent } from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

class Button extends PureComponent {
    render() {
        const {backgroundColor, text, onClick} = this.props;
        
        const style = {
            backgroundColor 
        }
        
        return (
            <button className="btn" style={style} onClick={onClick}>
                {text}
            </button>
        )
    }
}

Button.propTypes = {
    backgroundColor: PropTypes.string, 
    text: PropTypes.string.isRequired, 
    onClick: PropTypes.func.isRequired
}

Button.defaultProps = {
    backgroundColor: 'inherit', 
}

export default Button;