import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Modal from './components/Modal/Modal';
import Button from './components/Button/Button';
import Navbar from './components/Navbar/Navbar';
import AppRoutes from './routes/AppRoutes';
import Error from './pages/Error/Error';

import { getDataFromStorage } from './store/data/dataActions';
import { getCartFromStorage, updateCart} from './store/cart/cartActions';
import { getFavoritesFromStorage } from './store/favorites/favoritesActions';
import { closeModalWarning } from './store/modal/modalActions';


import './App.scss';


const App = (props) => {
    
    const {getDataFromStorage, getCartFromStorage, getFavoritesFromStorage} = props;
    const {isLoading, isError, errorMessage, updateCart, closeModalWarning} = props;
    const {isShowModal, action, id, name, vendorcode} = props.modalWarning;


    //Загрузка данных
    useEffect(() => {
      getDataFromStorage();
      getCartFromStorage();
      getFavoritesFromStorage();
    }, [getDataFromStorage, getCartFromStorage, getFavoritesFromStorage])

    //Кнопки в модалке updateCart
    const modalActions = [
      (action === 'ADD') && 
        <Button 
          key='0' 
          backgroundColor='#aaa' 
          text='Add 1 piece to Cart' 
          onClick={() => {
            updateCart({id: id, count: 1});
            closeModalWarning();
            }
          }
        />,
      (action === 'DELETE') && 
        <Button 
          key='0' 
          backgroundColor='#aaa' 
          text='Delete 1 piece from Cart' 
          onClick={() => {
            updateCart({id: id, count: -1});
            closeModalWarning();
            }
          }
        />,
      <Button 
        key='1' 
        backgroundColor='#aaa' 
        text='Cancel' 
        onClick={closeModalWarning}
      />,
      (action === 'DELETE ALL') && 
        <Button 
          key='9' 
          backgroundColor='#aaa' 
          text='Delete all pieces from Cart' 
          onClick={() => {
            updateCart({id: id, count: -999999999});
            closeModalWarning();
            }
          }
        />
    ]; 
    
    if (isLoading) return <div>Loading...</div>;

    return (
      <div className="App">
        <Navbar />
        {!isError ? <AppRoutes/> : <Error errorMessage={errorMessage}/>}
        {isShowModal &&
          <Modal 
            header='Do you realy want to change cart?' 
            closeButton={true} 
            text={`Action: ${action}\nName: ${name}\nVendor code: ${vendorcode}`}
            actions={modalActions} 
            onCloseClick={closeModalWarning}
          />
        }
        )
      </div>
    );

}

const mapStoreToProps = ({data: {isLoading, isError, errorMessage}, modalWarning}) => {
  return {
    isLoading,
    isError,
    errorMessage,
    modalWarning
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getDataFromStorage: (data) => dispatch(getDataFromStorage(data)),
    getCartFromStorage: () => dispatch(getCartFromStorage()),
    getFavoritesFromStorage: () => dispatch(getFavoritesFromStorage()),
    updateCart: ({id, count}) => dispatch(updateCart({id, count})),
    closeModalWarning: () => dispatch(closeModalWarning())
  }
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(App);
