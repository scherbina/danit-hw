import React from 'react'
import { connect } from 'react-redux';
import CardList from '../../components/CardList/CardList';
import { selectFullDataCards } from '../../store/data/dataSelector';

const Catalog = (props) => {
    const {data} = props;

    const cardActions = ['ADD']

  return (
    <>
      <h4 className='cardlist-title'>Catalog</h4>
      <CardList data={data} cardActions={cardActions}/>
    </>
  )
}

const mapStoreToProps = (store) => {
  return {
      data: selectFullDataCards(store)
  }
}

export default connect(mapStoreToProps)(Catalog);