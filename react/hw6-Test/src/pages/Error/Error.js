import React from 'react';
import { connect } from 'react-redux';
import './Error.scss';
import error_img from '../../theme/images/error.gif';
import { getDataFromStorage } from '../../store/data/dataActions';
import { Link } from 'react-router-dom';
import AddLineBreaks from '../../components/Tools/Tools';

const Error = (props) => {
  const {errorMessage, getDataFromStorage} = props;
  return (
    <div className='error'>
      <img className='error__img' src={error_img} alt='Error'></img>
      <h2>Something went wrong</h2>
      <p className='error__message'><strong><AddLineBreaks text={errorMessage}/></strong></p>

      <h3>But we already sent droids to fix it</h3>
      <Link to='/' onClick={getDataFromStorage}>Try reload</Link>
    </div>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    getDataFromStorage: () => dispatch(getDataFromStorage()),
  }
}

export default connect(null, mapDispatchToProps)(Error)