import React from 'react'
import  { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import CardList from '../../components/CardList/CardList';
import { selectFullDataCardsInCart } from '../../store/data/dataSelector';
import { clearCart } from '../../store/cart/cartActions';
import { clearCartForm } from '../../store/cart/cartFormActions';
import CartForm from '../../Forms/CartForm/CartForm';

const Cart = (props) => {
  const {data, clearCart, clearCartForm, history} = props;
  const cartLength = data.length;

  const onCheckout = (...props) => {
    console.log('Form Data', ...props);
    console.log('In Cart', data);
    clearCartForm();
    clearCart();
    history.push('/')
  }

  const cardActions = ['DELETE', 'DELETE ALL']

  return (
    <>
      <h4 className='cardlist-title'>Cart</h4>
      <CardList data={data} cardActions={cardActions}/>
      {/* <CartForm onSubmit={onCheckout} /> */}
      {cartLength > 0 ? <CartForm onSubmit={onCheckout} /> : null}
    </>
  )
}

const mapStoreToProps = (store) => {
  return {
    data: selectFullDataCardsInCart(store),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    clearCart: () => dispatch(clearCart()),
    clearCartForm: () => dispatch(clearCartForm()),
  }
}

export default connect(mapStoreToProps, mapDispatchToProps)(withRouter(Cart));