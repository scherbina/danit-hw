import React from 'react'
import { connect } from 'react-redux';
import CardList from '../../components/CardList/CardList';
import { selectFullDataFavoriteCards } from '../../store/data/dataSelector';

const Favorites = (props) => {
    const {data} = props;

    const cardActions = ['ADD']

    return (
      <>
        <h4 className='cardlist-title'>Favorites</h4>
        <CardList data={data} cardActions={cardActions}/>
      </>
    )
}

const mapStoreToProps = (store) => {
  return {
      data: selectFullDataFavoriteCards(store),
  }
}

export default connect(mapStoreToProps)(Favorites);