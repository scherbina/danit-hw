import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Navbar from './Navbar';
import { render } from '@testing-library/react';

describe('Testing Navbar component', () => {
    

    test(`Navbar is rendered correctly`, () => {
      const {container, getByText} = render(<BrowserRouter><Navbar /></BrowserRouter>);
      expect(container.querySelector('.navbar')).toBeInTheDocument();
      expect(container).toMatchSnapshot();
    })

    test(`Main link is rendered correctly`, () => {
      const {getByText} = render(<BrowserRouter><Navbar /></BrowserRouter>);
      const linkMain = getByText('Main');
      expect(linkMain).toBeInTheDocument();
      expect(linkMain.getAttribute('href')).toEqual('/');
    })

    test(`Cart link is rendered correctly`, () => {
      const {getByText} = render(<BrowserRouter><Navbar /></BrowserRouter>);
      const linkCart = getByText('Cart');
      expect(linkCart).toBeInTheDocument();
      expect(linkCart.getAttribute('href')).toEqual('/cart');
    })

    test(`Favorites link is rendered correctly`, () => {
      const {getByText} = render(<BrowserRouter><Navbar /></BrowserRouter>);
      const linkFavorites = getByText('Favorites');
      expect(linkFavorites).toBeInTheDocument();
      expect(linkFavorites.getAttribute('href')).toEqual('/favorites');
    })
})