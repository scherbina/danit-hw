import React from 'react';
import { NavLink } from 'react-router-dom';
import './Navbar.scss';

const Navbar = () => {
    return (
      <>
        <ul className='navbar'>
          <li className='navbar-item'><NavLink className='navbar-link' activeClassName='navbar-link-active' exact to='/'>Main</NavLink></li>
          <li className='navbar-item'><NavLink className='navbar-link' activeClassName='navbar-link-active' exact to='/cart'>Cart</NavLink></li>
          <li className='navbar-item'><NavLink className='navbar-link' activeClassName='navbar-link-active' exact to='/favorites'>Favorites</NavLink></li>
        </ul>
      </>
    )
}

export default Navbar