import React from 'react';
import { connect } from 'react-redux';
import Button from '../Button/Button';
import Icon from '../Icon/Icon';
import './Card.scss';

import { toogleFavorites } from '../../store/favorites/favoritesActions';
import { showModalWarning } from '../../store/modal/modalActions';

export const Card = (props) => {
        const {showModalWarning, onToogleFavorites, cardActions} = props;
        const {id, name, image, vendorcode, color, price, countInCart, isFavorite} = props.card;

        const actions = cardActions.map(action => {
            switch (action) {
                case 'ADD': 
                    return (
                        <Button 
                            key='ADD'
                            backgroundColor='#ccc' 
                            text='Add 1 to CART...' 
                            onClick={() => showModalWarning({action: 'ADD', id, name, vendorcode})}
                        />
                    )
                case 'DELETE': 
                    return (
                        <Button 
                            key='DELETE' 
                            backgroundColor='#ccc' 
                            text='Delete 1 from CART...' 
                            onClick={() => showModalWarning({action: 'DELETE', id, name, vendorcode})}
                        />
                    )
                case 'DELETE ALL':
                    return (
                        <Button 
                            key='DELETE ALL' 
                            className='card-btn-delete' 
                            backgroundColor='transparent' 
                            text='X' 
                            onClick={() => showModalWarning({action: 'DELETE ALL', id, name, vendorcode})}
                        />
                    )
                default: 
                    return null
            }
        })

        return (
            <div className='card'>
                <img className='card-image' src={image} alt={`${name} ${vendorcode}`}/>
                <div className='card-title'>
                    <span className='card-name'>{name}</span>
                    <Icon type='star' filled={isFavorite} className='card-favourite' onClick={onToogleFavorites.bind(null, id)}/>
                </div>
                <p className='card-vendorcode'>{vendorcode}</p>
                <p className='card-color'>Color: {color}</p>
                <p className='card-price'>{price} UAH</p>
                <p className='card-countInCart'>In CART: {countInCart}</p>
                {actions}
            </div>
        );
}

const mapDispatchToProps = dispatch => {
    return {
        onToogleFavorites: (id) => dispatch(toogleFavorites(id)),
        showModalWarning: itemUpdateCart => dispatch(showModalWarning(itemUpdateCart))
    }
}

export default connect(null, mapDispatchToProps)(Card);