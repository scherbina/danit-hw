import React from 'react';
import { Card } from './Card';
import { fireEvent, render } from '@testing-library/react';

const cardMock = {
    id: 1, 
    name: 'card Name',
    image: 'link to image',
    vendorcode: 'vendor code',
    color: 'card color',
    price: 12.34,
    countInCart: 2
}

const cardActions = ['ADD','DELETE','DELETE ALL'];
const showModalWarningMock = jest.fn();
const onToogleFavoritesMock = jest.fn();

describe('Testing Card component. ', () => {

    test(`Card is rendered correctly.`, () => {
      const {container} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
      expect(container.querySelector('.card')).toBeInTheDocument();
      expect(container).toMatchSnapshot();
    })

    test(`Card Image is rendered correctly.`, () => {
        const {container} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
        const image = container.querySelector('.card-image');
        expect(image).toBeInTheDocument();
        expect(image.getAttribute('src')).toBe(cardMock.image);
    })

    test(`Name is rendered correctly.`, () => {
        const {container} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
        const name = container.querySelector('.card-name');
        expect(name).toBeInTheDocument();
        expect(name.textContent).toBe(cardMock.name);
    })

    test(`isFavorite is rendered correctly.`, () => {
        const {container} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
        const isFavorite = container.querySelector('.card-favourite');
        expect(isFavorite).toBeInTheDocument();
        fireEvent.click(isFavorite);
        expect(onToogleFavoritesMock).toHaveBeenCalled();
    })

    test(`VendorCode is rendered correctly.`, () => {
        const {container} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
        const vendorcode = container.querySelector('.card-vendorcode');
        expect(vendorcode).toBeInTheDocument();
        expect(vendorcode.textContent).toBe(cardMock.vendorcode);
    })

    test(`Color is rendered correctly.`, () => {
        const {container} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
        const color = container.querySelector('.card-color');
        expect(color).toBeInTheDocument();
        expect(color.textContent).toBe(`Color: ${cardMock.color}`);
    })

    test(`Price is rendered correctly.`, () => {
        const {container} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
        const price = container.querySelector('.card-price');
        expect(price).toBeInTheDocument();
        expect(price.textContent).toBe(`${cardMock.price} UAH`);
    })

    test(`CountInCart is rendered correctly.`, () => {
        const {container} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
        const countInCart = container.querySelector('.card-countInCart');
        expect(countInCart).toBeInTheDocument();
        expect(countInCart.textContent).toBe(`In CART: ${cardMock.countInCart}`);
    })

    describe('Actions.', () => {
        test(`ADD Actions is rendered correctly.`, () => {
            const {getByText} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
            const addButton = getByText('Add 1 to CART...');
            expect(addButton).toBeInTheDocument();

            fireEvent.click(addButton);
            expect(showModalWarningMock).toHaveBeenCalledTimes(1);
            expect(showModalWarningMock).toHaveBeenLastCalledWith({
                action: 'ADD', 
                id: cardMock.id, 
                name: cardMock.name, 
                vendorcode: cardMock.vendorcode
            })
        })

        test(`DELETE Actions is rendered correctly.`, () => {
            const {getByText} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
            const deleteButton = getByText('Delete 1 from CART...');
            expect(deleteButton).toBeInTheDocument();

            fireEvent.click(deleteButton);
            expect(showModalWarningMock).toHaveBeenCalledTimes(2);
            expect(showModalWarningMock).toHaveBeenLastCalledWith({
                action: 'DELETE', 
                id: cardMock.id, 
                name: cardMock.name, 
                vendorcode: cardMock.vendorcode
            })
        })

        test(`DELETE ALL Actions is rendered correctly.`, () => {
            const {getByText} = render(<Card card={cardMock} cardActions={cardActions} showModalWarning={showModalWarningMock} onToogleFavorites={onToogleFavoritesMock}/>); 
            const deleteAllButton = getByText('X');
            expect(deleteAllButton).toBeInTheDocument();

            fireEvent.click(deleteAllButton);
            expect(showModalWarningMock).toHaveBeenCalledTimes(3);
            expect(showModalWarningMock).toHaveBeenLastCalledWith({
                action: 'DELETE ALL', 
                id: cardMock.id, 
                name: cardMock.name, 
                vendorcode: cardMock.vendorcode
            })
        })
    })
})