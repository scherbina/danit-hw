import React from 'react';
import Button from './Button';
import { unmountComponentAtNode, render } from 'react-dom';
import { act } from 'react-dom/test-utils'; 

let container = null;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
})

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
}) 

describe('Testing Button component', () => {
    // test('Header is rendered correctly', () => {
    //   act(() => {
    //     render(<Provider store={mockStore}><Header /></Provider>, container);
    //   })
    // })
  
    test('Button is rendered correctly', () => {
      const onClick = jest.fn();
      
      act(() => {
        render(<Button text="SomeText" backgroundColor="red" onClick={onClick}/>, container);
      })
      
      const button = container.querySelector('button');

      expect(button.textContent).toEqual('SomeText');
      expect(button.getAttribute("class")).toEqual('btn');
      expect(button.getAttribute("style")).toEqual('background-color: red;');

      act(() => {
        for (let i = 0; i < 5; i++) {
          button.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        }
      });

      expect(onClick).toHaveBeenCalledTimes(5);
      
    })
  })
