import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';

const Button = (props) => {
        const {backgroundColor, text, onClick, className} = props;
        
        const style = {
            backgroundColor 
        }

        const classList = className ? `btn ${className}` : 'btn'
        
        return (
            <button className={classList} style={style} onClick={onClick}>
                {text}
            </button>
        )
}

Button.propTypes = {
    backgroundColor: PropTypes.string, 
    text: PropTypes.string.isRequired, 
    onClick: PropTypes.func.isRequired
}

Button.defaultProps = {
    backgroundColor: 'inherit' 
}

export default Button;