import React from 'react';
import Modal from './Modal';
import { unmountComponentAtNode, render } from 'react-dom';
import { act } from 'react-dom/test-utils'; 

let container = null;

beforeEach(() => {
  container = document.createElement('div');
  document.body.appendChild(container);
})

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
}) 

describe('Testing Modal component', () => {

    test('Modal with Close button', () => {
      const onCloseClick = jest.fn();
      const actions = [<div key="1">ModalActions</div>]

      act(() => {
        render(<Modal header="ModalHeader" text="ModalText" actions={actions} closeButton={true} onCloseClick={onCloseClick} />, container);
      })

      const modal = container.querySelector('.modal');
      const modalCloseButton = container.querySelector('.modal-header button');

      //Есть заголовок формы с кнопокой Close
      expect(container.querySelector('.modal-header').textContent).toEqual('ModalHeaderX');
      //Еще одна проверка на наличие кнопки Close
      expect(modalCloseButton.textContent).toEqual('X');
      //Есть текст формы
      expect(container.querySelector('.modal-text').textContent).toEqual('ModalText');
      //Есть акшены
      expect(container.querySelector('.modal-actions').textContent).toEqual('ModalActions');
      
      //Проверка кликов по модалке но вне .modal-content
      act(() => {
        for (let i = 0; i < 5; i++) {
          modal.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        }
      });
      expect(onCloseClick).toHaveBeenCalledTimes(5);

      //Проверка кликов на кнопку Close
      act(() => {
        for (let i = 0; i < 5; i++) {
          modalCloseButton.dispatchEvent(new MouseEvent("click", { bubbles: true }));
        }
      });
      expect(onCloseClick).toHaveBeenCalledTimes(10);
      
    })

    test('Modal without Close button', () => {
      const onCloseClick = jest.fn();
      
      act(() => {
        render(<Modal header="ModalHeader" text="ModalText" actions={[<div key="1"></div>]} closeButton={false} onCloseClick={onCloseClick} />, container);
      })
      
      expect(container.querySelector('.modal-header').textContent).toEqual('ModalHeader');

    })

    test('Modal with linebreak text', () => {
      const onCloseClick = jest.fn();
      
      act(() => {
        render(<Modal header="ModalHeader" text="Modal\nText" actions={[<div key="1"></div>]} closeButton={false} onCloseClick={onCloseClick} />, container);
      })
      
      expect(container.querySelector('.modal-text').textContent).toEqual('Modal\\nText');

    })

})
