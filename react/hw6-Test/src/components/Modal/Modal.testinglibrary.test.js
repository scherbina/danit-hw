import React from 'react';
import Modal from './Modal';
import { render, fireEvent } from '@testing-library/react';

describe('Testing Modal component', () => {

    test('Modal with Close button', () => {
      const onCloseClick = jest.fn();
      const actions = [<div key="1">ModalActions</div>]

      const {container} = render(<Modal header="ModalHeader" text="ModalText" actions={actions} closeButton={true} onCloseClick={onCloseClick} />);

      const modal = container.querySelector('.modal');
      const modalCloseButton = container.querySelector('.modal-header button');

      //Есть заголовок формы с кнопокой Close
      expect(container.querySelector('.modal-header').textContent).toEqual('ModalHeaderX');
      //Еще одна проверка на наличие кнопки Close
      expect(modalCloseButton.textContent).toEqual('X');
      //Есть текст формы
      expect(container.querySelector('.modal-text').textContent).toEqual('ModalText');
      //Есть акшены
      expect(container.querySelector('.modal-actions').textContent).toEqual('ModalActions');
      
      //Проверка кликов по модалке но вне .modal-content
      for (let i = 0; i < 5; i++) {
          fireEvent.click(modal);
      }
      expect(onCloseClick).toHaveBeenCalledTimes(5);

      //Проверка кликов на кнопку Close
      for (let i = 0; i < 5; i++) {
          fireEvent.click(modalCloseButton);
      }
      expect(onCloseClick).toHaveBeenCalledTimes(10);
      
    })

    test('Modal without Close button', () => {
      const onCloseClick = jest.fn();
      const {container} = render(<Modal header="ModalHeader" text="ModalText" actions={[<div key="1"></div>]} closeButton={false} onCloseClick={onCloseClick} />);
      expect(container.querySelector('.modal-header').textContent).toEqual('ModalHeader');

    })

    test('Modal with linebreak text', () => {
      const onCloseClick = jest.fn();
      const {container} = render(<Modal header="ModalHeader" text="Modal\nText" actions={[<div key="1"></div>]} closeButton={false} onCloseClick={onCloseClick} />);
      expect(container.querySelector('.modal-text').textContent).toEqual('Modal\\nText');

    })

})
