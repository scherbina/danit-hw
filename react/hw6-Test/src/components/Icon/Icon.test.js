import React from 'react';
import Icon from './Icon';
import { render, fireEvent } from '@testing-library/react';

describe('Testing Icon component', () => {
  const onClickMock = jest.fn();

  test(`Icon without type dont rendered`, () => {
    const {container} = render(<Icon />);
    expect(container.querySelector('.icon')).not.toBeInTheDocument();
  })

  test('Icon with wrong type dont rendered', () => {
    const {container} = render(<Icon type="someIcon"/>);
    expect(container.querySelector('.icon')).not.toBeInTheDocument();
  })

  describe(`Testing Icon STAR component`, () => {
    const iconType = 'star';
    const iconDefaultColor = "#ffc107";
  
    test(`Icon ${iconType.toUpperCase()} rendered correct`, () => {
        const {container} = render(<Icon type="star"/>);
        expect(container.querySelector('.icon')).toBeInTheDocument();
    })
  
    test(`Icon ${iconType.toUpperCase()} with filled and color props present`, () => {
      const iconColor = 'red';
  
      const {container} = render(<Icon type={iconType} filled color={iconColor}/>);
      const svgPath = container.querySelector('svg path')
      expect(svgPath.getAttribute('fill')).toEqual("red");
      expect(svgPath.getAttribute('stroke')).toEqual("none");
      expect(container).toMatchSnapshot();
    })
  
    test(`Icon ${iconType.toUpperCase()} without filled and color props`, () => {
      const {container} = render(<Icon type={iconType}/>);
      const svgPath = container.querySelector('svg path')
      expect(svgPath.getAttribute('fill')).toEqual("none");
      expect(svgPath.getAttribute('stroke')).toEqual(iconDefaultColor);
    })
  
    test(`Icon ${iconType.toUpperCase()} with color withoud filled props`, () => {
      const iconColor = 'red';
  
      const {container} = render(<Icon type={iconType} color={iconColor}/>);
      const svgPath = container.querySelector('svg path')
      expect(svgPath.getAttribute('fill')).toEqual("none");
      expect(svgPath.getAttribute('stroke')).toEqual(iconColor);
    })
  
    test(`Icon ${iconType.toUpperCase()} without color with filled props`, () => {
      const {container} = render(<Icon type={iconType} filled/>);
      const svgPath = container.querySelector('svg path')
      expect(svgPath.getAttribute('fill')).toEqual(iconDefaultColor);
      expect(svgPath.getAttribute('stroke')).toEqual("none");
    })
  
    test(`Icon ${iconType.toUpperCase()} with className props`, () => {
      const iconClassName = 'iconClassName';
  
      const {container} = render(<Icon type={iconType} className={iconClassName}/>);
      expect(container.querySelector(`.${iconClassName}`)).toBeInTheDocument();
    })

    test(`Icon ${iconType.toUpperCase()} with onClick props and clicked on it`, () => {
      const {container} = render(<Icon type={iconType} onClick={onClickMock}/>);
      fireEvent.click(container.querySelector('.icon'));
      expect(onClickMock).toHaveBeenCalled();
    })
  
  })
})
