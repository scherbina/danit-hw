import React from 'react';
import * as icons from '../../theme/icons';
import classNames from 'classnames';
import './Icon.scss';


function Icon(props) {
  const { type, filled, color, className, onClick } = props;

  const iconJsx = icons[type];

  if (!iconJsx) {
    return null;
  }

  return (
    <span className={classNames('icon',{[`icon--${type}`]: !!type}, {[className]: !!className})} onClick={onClick}>
      {iconJsx(filled, color)}
    </span>
  )
}

export default Icon
