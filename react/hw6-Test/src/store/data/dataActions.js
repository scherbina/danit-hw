import axios from 'axios';

export const GET_DATA_REQUEST = 'GET_DATA_REQUEST';
export const GET_DATA_SUCCESS = 'GET_DATA_SUCCESS';
export const GET_DATA_ERROR = 'GET_DATA_ERROR';

export function getDataFromStorage() {
    return dispatch => {
      dispatch(getDataRequest())
    
      axios("/goods.json")
        .then(response => {
          if (response.status === 200) {
            return dispatch(getDataSuccess(response.data));
          }
        })
        .catch(err => {
          dispatch(getDataError(err.message));
        });
  }
}

const getDataRequest = () => ({
  type: GET_DATA_REQUEST
})

const getDataSuccess = (data) => ({
  type: GET_DATA_SUCCESS,
  payload: data
})

const getDataError = (message) => ({
  type: GET_DATA_ERROR,
  payload: message
})

