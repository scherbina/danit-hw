import dataReducer from "./dataReducer";
import { GET_DATA_REQUEST, GET_DATA_SUCCESS, GET_DATA_ERROR } from './dataActions';

const initialStore = {
    data: [],
    isLoading: false,
    isError: false,
    errorMessage: ''
}

describe('Test data Reducers', () => {
    test('GET_DATA_REQUEST switches isLoading to true', () => {
        const action = { type: GET_DATA_REQUEST }
        const newStore = dataReducer(initialStore, action);
      
        expect(newStore.isLoading).toBeTruthy();
        expect(newStore.data).toEqual(initialStore.data) 
    })
    
    test('GET_DATA_SUCCESS switches isLoading, isError to false, errorMessage to ""; store data; ', () => {
        const data = [{id: 1, name: 'name1'}, {id: 2, name: 'name2'}];
        const action = { type: GET_DATA_SUCCESS, payload: data }
        const newStore = dataReducer(initialStore, action);
      
        expect(newStore.isLoading).toBeFalsy();
        expect(newStore.isError).toBeFalsy();
        expect(newStore.errorMessage).toBe('');
        expect(newStore.data).toHaveLength(2);
        expect(newStore.data).toEqual(data) 
    })
    
    test('GET_DATA_ERROR switches isLoading to false, isError to true, errorMessage contain "Error loading data from storage"', () => {
        const action = { type: GET_DATA_ERROR, payload: 'Error message text' }
        const newStore = dataReducer(initialStore, action);
      
        expect(newStore.isLoading).toBeFalsy();
        expect(newStore.isError).toBeTruthy();
        // expect(newStore.errorMessage).toEqual(expect.stringContaining('Error loading data from storage'));
        expect(newStore.errorMessage).toMatch(/Error loading data from storage/);
        expect(newStore.data).toEqual(initialStore.data) 
    })
})
