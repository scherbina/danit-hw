import { selectFullDataCards, selectFullDataFavoriteCards, selectFullDataCardsInCart } from './dataSelector';

const initialStore = {
    data: {
        data: [
            {id: 1, name: 'name1'}, 
            {id: 2, name: 'name2'},
            {id: 3, name: 'name3'},
            {id: 4, name: 'name4'},
            {id: 5, name: 'name5'},
            {id: 6, name: 'name6'},
            {id: 7, name: 'name7'},
        ]
    },
    favorites: [1, 3, 6, 2],
    cart: [{id: 2, count: 1}, {id: 7, count: 2}, {id: 1, count: 3}]
}

describe('Test dataSelectors', () => {
    test('selectFullDataCards Merge data with favorites and cart', () => {
        const selected = selectFullDataCards(initialStore);
        expect(selected).toHaveLength(7);
        expect(selected).toEqual([
            {id: 1, name: 'name1', isFavorite: true, isInCart: true, countInCart: 3}, 
            {id: 2, name: 'name2', isFavorite: true, isInCart: true, countInCart: 1},
            {id: 3, name: 'name3', isFavorite: true, isInCart: false, countInCart: 0},
            {id: 4, name: 'name4', isFavorite: false, isInCart: false, countInCart: 0},
            {id: 5, name: 'name5', isFavorite: false, isInCart: false, countInCart: 0},
            {id: 6, name: 'name6', isFavorite: true, isInCart: false, countInCart: 0},
            {id: 7, name: 'name7', isFavorite: false, isInCart: true, countInCart: 2},
        ])
    })
    
    test('selectFullDataFavoriteCards filter FullData with Favorites only', () => {
        const selected = selectFullDataFavoriteCards(initialStore);
        expect(selected).toHaveLength(4);
        expect(selected).toEqual([
            {id: 1, name: 'name1', isFavorite: true, isInCart: true, countInCart: 3}, 
            {id: 2, name: 'name2', isFavorite: true, isInCart: true, countInCart: 1},
            {id: 3, name: 'name3', isFavorite: true, isInCart: false, countInCart: 0},
            {id: 6, name: 'name6', isFavorite: true, isInCart: false, countInCart: 0},
        ])
    })
    
    test('selectFullDataCardsInCart filter FullData with inCart only', () => {
        const selected = selectFullDataCardsInCart(initialStore);
        expect(selected).toHaveLength(3);
        expect(selected).toEqual([
            {id: 1, name: 'name1', isFavorite: true, isInCart: true, countInCart: 3}, 
            {id: 2, name: 'name2', isFavorite: true, isInCart: true, countInCart: 1},
            {id: 7, name: 'name7', isFavorite: false, isInCart: true, countInCart: 2},
        ])
    })
})
