export const UPDATE_CART_FORM = 'UPDATE_CART_FORM';
export const CLEAR_CART_FORM = 'CLEAR_CART_FORM';

export function updateCartForm(value) {
  return {
    type: UPDATE_CART_FORM,
    payload: value
  }
}

export function clearCartForm() {
  return {
    type: CLEAR_CART_FORM,
  }
}
