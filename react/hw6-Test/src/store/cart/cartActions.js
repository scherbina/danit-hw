export const GET_CART_FROM_STORAGE = 'GET_CART_FROM_STORAGE';
export const UPDATE_CART = 'UPDATE_CART';
export const CLEAR_CART = 'CLEAR_CART';

export function getCartFromStorage() {
    return {
      type: GET_CART_FROM_STORAGE
    }
}

export function updateCart({id, count}) {
  return {
    type: UPDATE_CART,
    payload: {id, count} 
  }
}

export function clearCart() {
  return {
    type: CLEAR_CART
  }
}


