import { UPDATE_CART_FORM, CLEAR_CART_FORM } from './cartFormActions';

const initialState = {
    firstName: '',
    lastName: '',
    age: '',
    addressDelivery: '',
    phoneMobile: '380',
    email: ''
};

const cartForm = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_CART_FORM:
            const {name, value} = action.payload;
            return {...state, [name]: value, dirty: true }
        case CLEAR_CART_FORM:
            return {...initialState}
        default:
            return state
    }
}

export default cartForm