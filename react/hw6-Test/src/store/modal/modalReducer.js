import { SHOW_MODAL, CLOSE_MODAL } from './modalActions';

const initialState = {
    isShowModal: false, 
    action: null,
    id: null, 
    name: null, 
    vendorcode: null
}

const modalWarning = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_MODAL: 
            return {...state, ...action.payload, isShowModal: true}
        case CLOSE_MODAL: 
            return {...initialState}
        default:
            return state
    }
    
}

export default modalWarning