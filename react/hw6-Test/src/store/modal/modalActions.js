export const SHOW_MODAL = 'SHOW_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export function showModalWarning(item) {
    return {
      type: SHOW_MODAL,
      payload: item,
    }
}

export function closeModalWarning() {
  return {
    type: CLOSE_MODAL
  }
}