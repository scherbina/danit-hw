import { combineReducers } from 'redux';

import data from './data/dataReducer';
import cart from './cart/cartReducer';
import cartForm from './cart/cartFormReducer';
import favorites from './favorites/favoritesReducer';
import modalWarning from './modal/modalReducer';

export default combineReducers ({
    data,
    favorites,
    cart,
    cartForm,
    modalWarning
})