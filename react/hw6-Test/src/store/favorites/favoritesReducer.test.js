import favoritesReducer from './favoritesReducer';
import { GET_FAVORITES_FROM_STORAGE, TOOGLE_FAVORITES } from './favoritesActions';

const initialStore = [1, 3, 7];
const favoritesInStorage = localStorage.hasOwnProperty('favourites') ? JSON.parse(localStorage.getItem('favourites')) : [];

describe('Test favoriteReducers', () => {
    test('GET_FAVORITES_FROM_STORAGE save favorites to Store', () => {
        const action = { type: GET_FAVORITES_FROM_STORAGE };
        const newStore = favoritesReducer(initialStore, action);

        expect(newStore).toHaveLength(favoritesInStorage.length);
        expect(newStore).toEqual(favoritesInStorage)
    })

    test('TOOGLE_FAVORITES remove favorite id=1', () => {
        const action = { type: TOOGLE_FAVORITES, payload: 1 };
        const newStore = favoritesReducer(initialStore, action);

        expect(newStore).toHaveLength(2);
        expect(newStore).toEqual([3,7])
    })

    test('TOOGLE_FAVORITES add favorite id=2', () => {
        const action = { type: TOOGLE_FAVORITES, payload: 2 };
        const newStore = favoritesReducer(initialStore, action);

        expect(newStore).toHaveLength(4);
        expect(newStore).toEqual([1, 3, 7, 2])
    })
})  


