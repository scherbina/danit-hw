import React from 'react';
import * as icons from '../../theme/icons';
import './Icon.scss';

function Icon(props) {
  const { type, filled, color, className, onClick } = props;

  const iconJsx = icons[type];

  if (!iconJsx) {
    return null;
  }

  return (
    <span className={`icon icon--${type} ${className}`} onClick={onClick}>
      {iconJsx(filled, color)}
    </span>
  )
}

export default Icon
