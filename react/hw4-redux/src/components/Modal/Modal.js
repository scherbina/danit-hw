import React from 'react';
import './Modal.scss';
import Button from '../Button/Button';
import AddLineBreaks from '../Tools/Tools';
import PropTypes from 'prop-types';



const Modal = (props) => {
        const {header, closeButton, text, actions, onCloseClick} = props;
        
        const onClick = ({target}) => {
            if (target.classList.contains('modal')) onCloseClick();
        }

        return (
            <div className="modal" onClick={onClick}>
                <div className="modal-content">
                    <div className="modal-header">{header}{closeButton && <Button text='X' onClick={onCloseClick}/>}</div>
                    <p className="modal-text"><AddLineBreaks text={text}/></p>
                    <div className="modal-actions">
                        {actions}
                    </div>
                </div>
            </div>
        )
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired,
    actions: PropTypes.array.isRequired,
    onCloseClick: PropTypes.func.isRequired
}

export default Modal;