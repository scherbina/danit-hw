import React from 'react'
import { connect } from 'react-redux';
import CardList from '../../components/CardList/CardList';
import { selectFullDataCardsInCart } from '../../store/data/dataSelector';

const Cart = (props) => {
  const {data} = props;

  const cardActions = ['DELETE', 'DELETE ALL']

  return (
    <>
      <h4 className='cardlist-title'>Cart</h4>
      <CardList data={data} cardActions={cardActions}/>
    </>
  )
}

const mapStoreToProps = (store) => {
  return {
    data: selectFullDataCardsInCart(store),  
  }
}

export default connect(mapStoreToProps)(Cart);