import { GET_DATA_REQUEST, GET_DATA_SUCCESS, GET_DATA_ERROR } from './dataActions';

const initialState = {
    data: [],
    isLoading: false,
    isError: false,
    errorMessage: ''
}

const data = (state = initialState, action) => {
    switch (action.type) {
        case GET_DATA_REQUEST:
            return {
                ...state,
                isLoading: true,
            }
        case GET_DATA_SUCCESS:
            return {
                ...state,
                data: action.payload,
                isLoading: false,
                isError: false,
                errorMessage: ''
            }
        case GET_DATA_ERROR:
            return {
                ...state,
                isLoading: false,
                isError: true,
                errorMessage: `Error loading data from storage.\n${action.payload}`
                }
        default:
            return state
    }
    
}

export default data