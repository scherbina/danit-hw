import { GET_FAVORITES_FROM_STORAGE, TOOGLE_FAVORITES } from './favoritesActions';

const initialState = [];

const favorites = (state = initialState, action) => {
    switch (action.type) {
        case GET_FAVORITES_FROM_STORAGE:
            return localStorage.hasOwnProperty('favourites') ? JSON.parse(localStorage.getItem('favourites')) : [];
        case TOOGLE_FAVORITES:
            const id = action.payload;
            const newFavourites = state.includes(id) ? state.filter(itemid => itemid !== id) : [...state, id];
            localStorage.setItem('favourites', JSON.stringify(newFavourites))
            return [...newFavourites];
        default:
            return state
    }
}

export default favorites