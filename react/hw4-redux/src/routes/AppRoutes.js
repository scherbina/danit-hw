import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Favorites from '../pages/Favorites/Favorites';
import Cart from '../pages/Cart/Cart';
import Catalog from '../pages/Catalog/Catalog';
import Error from '../pages/Error/Error'

const AppRoutes = () => {
    return (
        <Switch>
            <Route exact path='/' component={Catalog} />
            <Route exact path='/favorites' component={Favorites} />
            <Route exact path='/cart' component={Cart} />
            <Route path='*' render={() => <Error errorMessage={`Error loading page.\n404: Page not Found`}/>} />
        </Switch>
    )
}

export default AppRoutes