import React from 'react';
import { connect } from 'react-redux';
import FormField from '../FormField/FormField';
import { withFormik } from 'formik';
import * as yup from 'yup';
import './CartForm.scss';

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const validationSchema = yup.object().shape({
  firstName: yup
    .string()
    .required("Name is required field")
    .min(3, "Name must be at least 3 characters"),
  lastName: yup
    .string()
    .required("Last name is required field")
    .min(3, "Last name must be at least 3 characters"),
  age: yup
    .number()
    .required("Please write your age")
    .min(18, "You must be at least 18 years")
    .max(100, "You must be at most 100 years"),
  addressDelivery: yup
    .string()
    .required("Delivery address is required field")
    .min(8, "Minimum length is 8 symbols"),
  phoneMobile: yup
    .string()
    .required("Phone number is required field")
    .matches(phoneRegExp, 'Phone number is not valid. Use format 123456789012')
    .min(12, "Minimum length is 12 digits")
    .max(12, "Maximum length is 12 digits"),
  email: yup
    .string()
    .required("Email is required field")
    .email("Please write valid Emails"),
})

const CartForm = (props) => {
  const {onSubmit, isValid, cartFormValues: {dirty}, values} = props;

  const formSubmit = (event) => {
    event.preventDefault();
    onSubmit(values)
  }

  return (
    <form onSubmit={formSubmit}>
      <FormField
        label="First Name"
        type="text"
        name="firstName"
        placeholder="Please Enter your first name"
      />

      <FormField
        label="Last Name"
        type="text"
        name="lastName"
        placeholder="Please Enter your last name"
      />

      <FormField
        label="Age"
        type="number"
        name="age"
        placeholder="Please Enter your age"
      />

      <FormField
        label="Mobile phone"
        type="text"
        name="phoneMobile"
      />

      <FormField
        label="Delivery Address"
        type="text"
        name="addressDelivery"
        placeholder="Please Enter your address for delivery"
      />

      <FormField
        label="Email"
        type="email"
        name="email"
        placeholder="Please Enter your email"
      />

      <button type="submit" disabled={!(isValid && dirty)} onClick={formSubmit}>Checkout</button>
    </form>
  );
}

const mapStoreToProps = store => {
  return {
    cartFormValues: store.cartForm
  }
}

export default connect(mapStoreToProps)(withFormik({
  mapPropsToValues: ({cartFormValues: {firstName, lastName, age, addressDelivery, phoneMobile, email}}) => ({
    firstName,
    lastName,
    age,
    addressDelivery,
    phoneMobile,
    email,
  }),
  validationSchema,
})(CartForm));