import React from "react";
import { connect } from 'react-redux';
import { useField } from 'formik';
import { updateCartForm } from '../../store/cart/cartFormActions';

const FormField = (props) => {
  const {updateCartForm, name, label, ...rest} = props;
  const [field, meta] = useField(name);
  const {touched, error} = meta;

  const handleChange = (e) => {
    updateCartForm({name, value: e.target.value});
    field.onChange(e);
  }

  const fielderror = (touched && error) ? <div className='formError'>{error}</div> : null;
  
  return (
    <div >
      <label htmlFor={name}>{label}</label>
      <input {...field} {...rest} onChange={handleChange}/>
      {fielderror}
    </div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    updateCartForm: (data) => dispatch(updateCartForm(data))
  }
}

export default connect(null, mapDispatchToProps)(FormField);