import React from 'react';
import ReactDOM from 'react-dom';
import ErrorBoundary from './containers/ErrorBoundary/ErrorBoundary';
import Error from './pages/Error/Error';
import { BrowserRouter } from 'react-router-dom';
import store from './store/configureStore.js';
import { Provider } from 'react-redux';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <ErrorBoundary errorPage={<Error />}>
          <App />
        </ErrorBoundary>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
