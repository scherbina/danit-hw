import React from 'react';

const AddLineBreaks = (props) => {
    const {text} = props;
    return (
        text
            .split('\n')
            .map((string, index) => 
                (
                <React.Fragment key={`${string}-${index}`}>
                    {string}
                    <br />
                </React.Fragment>
                )
            )
    )
}

export default AddLineBreaks;
