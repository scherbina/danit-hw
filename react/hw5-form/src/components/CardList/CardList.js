import React from 'react';
import './CardList.scss';
import Card from '../Card/Card';

const CardList = (props) => {
        const {data, cardActions} = props;

        const cards = data.map(card => 
                <Card 
                    key={card.id} 
                    card={card} 
                    cardActions={cardActions}
                />
        )

        return (
            <>
               <ul className='cardlist'>
                    {cards}
               </ul>
            </>
        );
}

export default CardList;
