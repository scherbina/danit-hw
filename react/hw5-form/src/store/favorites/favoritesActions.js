export const GET_FAVORITES_FROM_STORAGE = 'GET_FAVORITES_FROM_STORAGE';
export const TOOGLE_FAVORITES = 'TOOGLE_FAVORITES';

export function getFavoritesFromStorage() {
  return {
    type: GET_FAVORITES_FROM_STORAGE
  }
}

export function toogleFavorites(id) {
  return {
    type: TOOGLE_FAVORITES,
    payload: id
  }
}