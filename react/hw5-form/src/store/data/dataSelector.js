import { createSelector } from 'reselect';

const selectCards = state => state.data.data;
const selectFavorites = state => state.favorites;
const selectCart = state => state.cart;

export const selectFullDataCards = createSelector (
    [selectCards, selectFavorites, selectCart],
    (cards, favorites, cart) => {
        return cards.map(card => {
            const cardInCart = cart.find(cartItem => cartItem.id === card.id)
            return {
                ...card,
                isFavorite: favorites.includes(card.id),
                isInCart: !!cardInCart,
                countInCart: (!!cardInCart && cardInCart.count) || 0
            }
        })
    }
)

export const selectFullDataFavoriteCards = createSelector(
    [selectFullDataCards, selectFavorites],
    (cards, favorites) => cards.filter( ({id}) => favorites.includes(id))
);

export const selectFullDataCardsInCart = createSelector(
    [selectFullDataCards, selectCart],
    (cards, cart) => {
        const idInCart = cart.map(cart => cart.id);
        return cards.filter( ({id}) => idInCart.includes(id));
    }
);
