import { GET_CART_FROM_STORAGE, UPDATE_CART, CLEAR_CART } from './cartActions';

const initialState = [];

const cart = (state = initialState, action) => {
    switch (action.type) {
        case GET_CART_FROM_STORAGE:
            return localStorage.hasOwnProperty('cart') ? JSON.parse(localStorage.getItem('cart')) : []
        case UPDATE_CART:
            const { id, count } = action.payload;
            let isInCart = false;
            let updatedCart = state.map(itemInCart => {
                if (itemInCart.id === id) {
                    isInCart = true;
                    return { id: itemInCart.id, count: itemInCart.count + count }
                }
                return { id: itemInCart.id, count: itemInCart.count }
            })
            if (count > 0 && !isInCart) updatedCart.push({ id, count });
            if (count < 0) updatedCart = updatedCart.filter(itemInCart => itemInCart.count > 0);
            localStorage.setItem('cart', JSON.stringify(updatedCart))
            return updatedCart
        case CLEAR_CART:
            localStorage.removeItem('cart');
            return []
        default:
            return state
    }
}

export default cart