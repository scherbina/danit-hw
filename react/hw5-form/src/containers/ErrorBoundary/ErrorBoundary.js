import { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';

class ErrorBoundary extends PureComponent {
  state = {
    hasError: false
  }

  componentDidUpdate(prevProps) {
    const { location } = this.props;

    if (prevProps.location !== location) {
      this.setState({ hasError: false });
    }
  }

  componentDidCatch(error, errorInfo) {
    console.log('componentDidCatch error', error);
    console.log('componentDidCatch errorInfo', errorInfo);
    // send log to server
    // this.setState({ hasError: true });
  }

  static getDerivedStateFromError(error) {
    // console.log('getDerivedStateFromError', error);
    return { hasError: true }
  }

  render() {
    const { hasError } = this.state;
    const { children, errorPage } = this.props;

    if (hasError) {
      return errorPage
    }

    return children;
  }
}

export default withRouter(ErrorBoundary)