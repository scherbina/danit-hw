class TrelloColumn {
    constructor(name) {
        this._init (name);

    }

    appendTo(parent) {
        parent.append(this._html.container)
    }

    addCard(card) {
        this._data.list.push(card);
    }

    cardsSort() {
        const cardsText = 
            [...this._html.list.querySelectorAll('.trello-column__card-text')]
            .map(item => {
                const text = item.value;
                item.remove();
                return text
            })
            .sort();
            
        cardsText.forEach(text => this._addCardtoUI(text));
    }

    _init(name = 'Без названия') {
        this._data = {
            title: name,    //Заголовок колонки
            // list: []        //Список карточек
        };
        this._html = {}

        this._CreateTrelloColumnUI();
        
        this._cardDragStartBound = this._cardDragStart.bind(this);
        this._cardDragOverBound = this._cardDragOver.bind(this);
        this._cardDragEndBound = this._cardDragEnd.bind(this);
        
        this._cardsSortBound = this.cardsSort.bind(this);
        this._html.btnSort.addEventListener('click', this._cardsSortBound);
        
        this._createNewCardBound = this._createNewCard.bind(this);
        this._html.btnAdd.addEventListener('click', this._createNewCardBound);
    }


    _CreateTrelloColumnUI() {
        //***Контейнер колонки
        this._html.container = document.createElement('div');
        this._html.container.classList.add('trello-column');

        //***Обертка заголовка
        this._html.header = document.createElement('div');
        this._html.header.classList.add('trello-column__header');
        this._html.container.append(this._html.header);

        //***Название колонки
        this._html.title = document.createElement('p');
        this._html.title.classList.add('trello-column__title');
        this._html.title.innerText = this._data.title;
        this._html.header.append(this._html.title);

        //***Кнопка сортировки
        this._html.btnSort = document.createElement('button');
        this._html.btnSort.classList.add('trello-column__btnSort', 'btn');
        this._html.btnSort.innerText = 'Sort';
        this._html.header.append(this._html.btnSort);

        //***Пустой список
        this._html.list = document.createElement('ul');
        this._html.list.classList.add('trello-column__list');
        this._html.container.append(this._html.list);

        //***Кнопка добавления карточек
        this._html.btnAdd = document.createElement('button');
        this._html.btnAdd.classList.add('trello-column__btnAdd', 'btn');
        this._html.btnAdd.innerText = 'Добавить карточку...';
        this._html.container.append(this._html.btnAdd);
    }

    //Добавление новой карточки
    _createNewCard() {
        this._addCardtoUI(null);
    }

    _addCardtoUI(text) {
        const elCard = document.createElement('li');
        elCard.classList.add('trello-column__card');
        const elCardText = document.createElement('input');
        elCardText.type = 'text';
        elCardText.placeholder = 'Введите текст для карточки в это поле...'
        elCardText.value = text;
        elCardText.classList.add('trello-column__card-text');
        elCard.append(elCardText);
        elCard.draggable = true;
        this._html.list.append(elCard);

        elCard.addEventListener('dragstart', this._cardDragStartBound);
        elCard.addEventListener('dragover', this._cardDragOverBound);
        elCard.addEventListener('dragend', this._cardDragEndBound);
    }

    _cardDragStart(e) {
        e.dataTransfer.effectAllowed = 'move'
        e.dataTransfer.setData('text/plain', null)
        this._html.draggedCard = e.target;
    }

    _cardDragOver(e) {
        function isBefore(el1, el2) {       //Возвращает true, если el2 предыдущий сосед у el1, в обратном случае возвращает false
            if (el1.parentNode === el2.parentNode) {
                for (let elCurent = el1.previousSibling; elCurent; elCurent = elCurent.previousSibling) {
                    if (elCurent === el2) return true
                }
            }
            return false;
        }

        const target = e.target.closest('li');

        if (isBefore(this._html.draggedCard, target)) {
            target.parentNode.insertBefore(this._html.draggedCard, target)
          } else {
            target.parentNode.insertBefore(this._html.draggedCard, target.nextSibling)
        }

    }

    _cardDragEnd(e) {
        this._html.draggedCard = null;
    }

}

const trello1 = new TrelloColumn('Колонка с Drag-And-Drop')
trello1.appendTo(document.body);

const trello2 = new TrelloColumn('Колонка с Drag-And-Drop')
trello2.appendTo(document.body);

const trello3 = new TrelloColumn('Колонка с Drag-And-Drop')
trello3.appendTo(document.body);
