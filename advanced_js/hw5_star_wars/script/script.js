class StarWars {
    constructor() {
        this._init();
        this._createUI();
        this._addListiner();
    }

    _init() {
        this._data = {};
        this._data.URL = 'https://swapi.dev/api/films/';
        this._html = {};
        this._html.container = document.createElement('div');
        this._html.container.classList.add('starwars');
        
    }

    _createUI() {
        this._html.title = document.createElement('h4');
        this._html.title.classList.add('starwars__title');
        this._html.title.innerText = 'StarWars films';
        this._html.container.append(this._html.title);

        this._html.btnLoad = document.createElement('button');
        this._html.btnLoad.classList.add('starwars__btnLoad');
        this._html.btnLoad.innerText = 'Load...';
        this._html.container.append(this._html.btnLoad);
    }

    _renderFilms() {
        this._html.films = document.createElement('ul');
        this._html.films.classList.add('starwars__films');
        this._html.container.append(this._html.films);
        
    }

    _renderFilm(film, id) {
        const filmEl = document.createElement('li');
        filmEl.classList.add('starwars__film');
        const filmTitleEl = document.createElement('h4');
        const filmCrawlEl = document.createElement('p');
        filmTitleEl.innerText = `Episod ${film.episode_id}. ${film.title}`;
        filmCrawlEl.innerText = film.opening_crawl;
        filmEl.append(filmTitleEl);
        filmEl.append(filmCrawlEl);
        this._html.films.append(filmEl);

        const charactersEl = document.createElement('ul');
        charactersEl.classList.add('starwars__characters');
        charactersEl.innerText = 'Characters:';
        filmEl.append(charactersEl);

        this._html.films[id] = {
            film: filmEl, 
            filmTitle: filmTitleEl, 
            filmCrawl: filmCrawlEl, 
            characters: charactersEl
        };
    }

    _renderCharacter(character, filmID) {
        const characterEl = document.createElement('li');
        characterEl.classList.add('starwars__character');
        characterEl.innerText = character;
        this._html.films[filmID].characters.append(characterEl);
    }

    _clearFilms() {
        if (this._html.films) this._html.films.remove();
    }

    _renderSpinner(id) {
        const spinEl = document.createElement('div');
        spinEl.classList.add('lds-ellipsis');
        spinEl.innerHTML = '<div></div><div></div><div></div><div></div>';
        this._html.films[id].spinner = spinEl;
        this._html.films[id].filmTitle.after(spinEl);
    }

    _renderInlineSpinner(element) {
        const spinEl = document.createElement('i');
        spinEl.classList.add('c-inline-spinner');
        element.append(spinEl);
        return spinEl
    }

    _removeSpinner(spinner) {
        spinner.remove();
    }

    _addListiner() {
        this._loadFilmsBound = this._loadFilms.bind(this);
        this._html.btnLoad.addEventListener('click', this._loadFilmsBound);
    }

    _loadFilms() {
        this._html.btnLoad.disabled = true;
        this._clearFilms();
        this._data.filmsFullLoaded = 0;
        this._onLoadBound = this._onLoad.bind(this);
        this._onErrorBound = this._onError.bind(this);
        this._html.spinner = this._renderInlineSpinner(this._html.btnLoad);
        this._ajax('GET', this._data.URL, this._onLoadBound, this._onErrorBound)
    }

    _loadCharacters(id) {
        this._onLoadCharacterBound = this._onLoadCharacter.bind(this, id);
        this._onErrorCharacterBound = this._onErrorCharacter.bind(this);
        
        this._data.films[id].loadedCharacters = [];
        
        this._data.films[id].characters.forEach(character => {
            this._ajax('GET', character, this._onLoadCharacterBound, this._onErrorCharacterBound)
        })
        
    }

    _onLoad(respons) {
        if (respons.count > 0 ) {
            this._data.films = respons.results;
            
            this._renderFilms();

            this._data.films.forEach((film, id) => {
                this._renderFilm(film, id);
                this._html.films[id].spinner = this._renderInlineSpinner(this._html.films[id].filmTitle);
                this._loadCharacters(id);
            });
        }
    }

    _onError(status, response) {
        throw(`Error load films. ErrorCode: ${status}`);
    }

    _onLoadCharacter(id, respons) {
        this._data.films[id].loadedCharacters.push(respons);
        
        if (this._data.films[id].characters.length === this._data.films[id].loadedCharacters.length) {
            this._removeSpinner(this._html.films[id].spinner);
            this._data.filmsFullLoaded++;
            this._data.films[id].loadedCharacters.forEach(character => {
                this._renderCharacter(character.name, id);
            })
            if (this._data.films.length === this._data.filmsFullLoaded) {
                this._removeSpinner(this._html.spinner);
                this._html.btnLoad.disabled = false;
            }
        }
    }

    _onErrorCharacter(status, response) {
        throw(`Error load characters. ErrorCode: ${status}`);
    }


    appendTo(parent) {
        parent.append(this._html.container)
    }

    _ajax(method, URL, callback, fallback) {
        const req = new XMLHttpRequest();
        req.open(method, URL);
        req.onload = () => {
            if (req.readyState === 4 && req.status === 200) {
                callback(JSON.parse(req.response));
            }
            else if(fallback){
                fallback(req.status, req.responseText);
            }
        };
        req.send();
    }
}

const sw = new StarWars();
sw.appendTo(document.body);


