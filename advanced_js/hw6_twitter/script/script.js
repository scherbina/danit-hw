class MyTwitter {
    static URL = {
        api: 'https://jsonplaceholder.typicode.com/',
        posts: 'posts',
        users: 'users',
    }


    constructor() {
        this._init();
        this._createUI();
        this._getPosts();
        this._getUsers();
        this._addListiners();
    }

    _init() {
        this._html = {
            spinner: {}
        };
        this._data = {
            loads: 0,
            title: 'My Twitter App'
        };

        this._initSpinner();
        
    }

    _addListiners() {
        this._addNewPostBound = this._addNewPost.bind(this);
        this._html.btnAdd.addEventListener('click', this._addNewPostBound);

        this._postsActionBound = this._postsAction.bind(this);
        this._html.posts.addEventListener('click', this._postsActionBound);
    }

    _getUserOnId(userId) {
        return this._data.users.find(user => user.id === userId)
    }

    _getPostOnId(postId) {
        return this._data.posts.find(post => post.id === postId)
    }

    _addNewPost() {
        // const currentAuthor = {id: 1, name: 'YourFirstName YourLastName', email: 'yourmail@test.com.ua'};
        this._editPost();
    }

    _postsAction(e) {
        if (e.target.tagName === 'BUTTON') {
            const action = e.target.dataset.action;
            const postId = parseInt(e.target.closest('.my-twitter__post').dataset.postid);
            const post = this._data.posts.find(postItem => postItem.id === postId);
            switch (action) {
                case 'edit': this._editPost(post);
                    break;
                case 'delete': this._deleteData(post);
                    break
            }
        }
    }

    _destroyEditPost() {
        this._html.edit.btnCancel.removeEventListener('click', this._destroyEditPostBound);
        this._html.edit.btnSave.removeEventListener('click', this._saveEditPostBound);
        this._html.edit.form.remove();
        this._html.edit = {};
    }

    _saveEditPost(postId, event) {
        if (postId === 0) {
            const post = {
                title: this._html.edit.title.value,
                body: this._html.edit.body.value,
                userId: 1
            }
            this._postData(post);
        } else {
            const post = {
                userId: this._getPostOnId(postId).userId,
                id: postId,
                title: this._html.edit.title.value,
                body: this._html.edit.body.value
            }
            this._putData(post);
        }
    }

    _getPosts() {
        this._getData(MyTwitter.URL.posts);
    }

    _getUsers() {
        this._getData(MyTwitter.URL.users);
    }

    _getData(path) {
        this._data.loads++;
        this._addSpinner();
        $.getJSON(MyTwitter.URL.api + path)
            .done(data => {
                this._data[path] = data;
                this._data.loads--;
                this._onData.call(this, path);
            })
            .fail(() => {
                this._data.loads--;
            })
            .always(() => {
                this._removeSpinner();
            });

            // $.when(req1, req2).done(([response1], [response2]) => {

            // }) 
    }

    _postData(post) {
        this._data.loads++;
        this._addSpinner();
        $.ajax(MyTwitter.URL.api + MyTwitter.URL.posts, {
            method: 'POST',
            body: JSON.stringify(post),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
        })
        .done(({id}) => {
            this._data.loads--;
            this._data.posts = [...this._data.posts, {...post, id}];
            this._destroyEditPost();
            this._showPost({...post, id});
        })
        .fail(() => {
            this._data.loads--;
        })
        .always(() => {
            this._removeSpinner();
        });
    }

    _putData(changedPost) {
        this._data.loads++;
        this._addSpinner();
        $.ajax(MyTwitter.URL.api + MyTwitter.URL.posts + "/" + changedPost.id, {
            method: 'PUT',
            body: JSON.stringify(changedPost),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
        })
        .done(() => {
            this._data.loads--;
            this._data.posts = this._data.posts.map(oldPost => {
                if (oldPost.id === changedPost.id) {
                    return changedPost
                } else return oldPost;
            })
            this._destroyEditPost();
            this._refreshPost(changedPost);
        })
        .fail(() => {
            this._data.loads--;
        })
        .always(() => {
            this._removeSpinner();
        });
    }

    _deleteData(postToDelete) {
        this._data.loads++;
        this._addSpinner();
        $.ajax(MyTwitter.URL.api + MyTwitter.URL.posts + "/" + postToDelete.id, {
            method: 'DELETE',
        })
        .done(() => {
            this._data.loads--;
            this._data.posts = this._data.posts.filter(postItem => postItem.id !== postToDelete.id);
            this._removePostFromUI(postToDelete.id);
        })
        .fail(() => {
            this._data.loads--;
        })
        .always(() => {
            this._removeSpinner();
        });
    }

    _onData(path, data) {
        if (this._data.loads === 0) {
            this._showPosts();
        }
    }

    _showPosts() {
        this._data.posts.forEach(post => {
            this._showPost(post);
        })
    }

    _showPost(post) {
        const author = this._getUserOnId(post.userId);

        const elPost = document.createElement('div');
        elPost.classList.add('my-twitter__post');
        elPost.dataset.postid = post.id;

        const elTitle = document.createElement('h5');
        elTitle.classList.add('my-twitter__post-title');
        elTitle.innerText = post.title;
        elPost.append(elTitle);

        const elBody = document.createElement('p');
        elBody.classList.add('my-twitter__post-body');
        elBody.innerText = post.body;
        elPost.append(elBody);

        const elAuthor = document.createElement('div');
        elAuthor.classList.add('my-twitter__post-author');
        elPost.append(elAuthor);

        const elAuthorName = document.createElement('p');
        elAuthorName.classList.add('my-twitter__post-author-name');
        elAuthorName.innerText = `Author: ${author.name}`;
        elAuthor.append(elAuthorName);

        const elAuthorEmail = document.createElement('p');
        elAuthorEmail.classList.add('my-twitter__post-author-email');
        elAuthorEmail.innerHTML = `Email: <a href="mailto:${author.email}">${author.email}</a>`;
        elAuthor.append(elAuthorEmail);

        const elBtnWrapper = document.createElement('div');
        elBtnWrapper.classList.add('my-twitter__post-BtnWrapper');
        elPost.append(elBtnWrapper);

        const elBtnEdit = document.createElement('button');
        elBtnEdit.classList.add('my-twitter__btnEdit', 'btn');
        elBtnEdit.innerText = '...';
        elBtnEdit.dataset.action = 'edit';
        elBtnWrapper.append(elBtnEdit);

        const elBtnDelete = document.createElement('button');
        elBtnDelete.classList.add('my-twitter__btnDelete', 'btn');
        elBtnDelete.innerText = 'X';
        elBtnDelete.dataset.action = 'delete';
        elBtnWrapper.append(elBtnDelete);

        this._html.posts.prepend(elPost);
    }

    _refreshPost({id, title, body, userId}) {
        const author = this._getUserOnId(userId);
        document.querySelector(`.my-twitter__post[data-postid="${id}"] > .my-twitter__post-title`).innerText = title;
        document.querySelector(`.my-twitter__post[data-postid="${id}"] > .my-twitter__post-body`).innerText = body;
        document.querySelector(`.my-twitter__post[data-postid="${id}"] .my-twitter__post-author-name`).innerText = author.name;
        document.querySelector(`.my-twitter__post[data-postid="${id}"] .my-twitter__post-author-email`).innerText = author.email;
    }

    _removePostFromUI(postId) {
        document.querySelector(`.my-twitter__post[data-postid="${postId}"]`).remove();
    }

    _initSpinner() {
        const spinnerContainer = document.createElement('div');
        spinnerContainer.classList.add('my-twitter__spinner');
        const spinner = document.createElement('div');
        spinner.classList.add('loader');
        spinnerContainer.append(spinner);
        this._html.spinner = spinnerContainer;
    }

    _addSpinner() {
        if (this._data.loads === 1) {
            this._html.container.append(this._html.spinner);
            
        }
    }
    _removeSpinner() {
        if (this._data.loads === 0) {
            this._html.spinner.remove();
            
        }
    }

    _createUI() {
        //***Контейнер приложения
        this._html.container = document.createElement('div');
        this._html.container.classList.add('my-twitter')

        //***Обертка заголовка
        this._html.header = document.createElement('div');
        this._html.header.classList.add('my-twitter__header');
        this._html.container.append(this._html.header);

        //***Название колонки
        this._html.title = document.createElement('p');
        this._html.title.classList.add('my-twitter__title');
        this._html.title.innerText = this._data.title;
        this._html.header.append(this._html.title);

        //***Кнопка добавления новой публикации
        this._html.btnAdd = document.createElement('button');
        this._html.btnAdd.classList.add('my-twitter__btnAdd', 'btn');
        this._html.btnAdd.innerText = 'Добавить публикацию';
        this._html.header.append(this._html.btnAdd);

        //***Пустой список
        this._html.posts = document.createElement('ul');
        this._html.posts.classList.add('my-twitter__posts');
        this._html.container.append(this._html.posts);
    }

    appendTo(parent) {
        parent.append(this._html.container);
    }

    _editPost({id = 0, title = '', body = ''} = {id: 0, title: '', body: ''}) {

        const elModal = document.createElement('div');
        elModal.classList.add('my-twitter__modal');
        
        const elPost = document.createElement('div');
        elPost.classList.add('my-twitter__post', 'my-twitter__post--edit');
        elModal.append(elPost);

        const elTitle = document.createElement('input');
        elTitle.type = 'text';
        elTitle.placeholder = 'Название публикации...'
        elTitle.value = title;
        elTitle.classList.add('my-twitter__post-title--edit');
        elPost.append(elTitle);

        const elBody = document.createElement('textarea');
        elBody.placeholder = 'Текст публикации...'
        elBody.value = body;
        elBody.rows = "5";
        elBody.classList.add('my-twitter__post-body--edit');
        elPost.append(elBody);

        const elBtnWrapper = document.createElement('div');
        elBtnWrapper.classList.add('my-twitter__post-BtnWrapper--edit');
        elPost.append(elBtnWrapper);

        const elBtnSave = document.createElement('button');
        elBtnSave.classList.add('my-twitter__btnSave', 'btn');
        elBtnSave.innerText = 'Сохранить';
        elBtnWrapper.append(elBtnSave);

        const elBtnCancel = document.createElement('button');
        elBtnCancel.classList.add('my-twitter__btnCancel', 'btn');
        elBtnCancel.innerText = 'Отменить';
        elBtnWrapper.append(elBtnCancel);

        this._saveEditPostBound = this._saveEditPost.bind(this, id);
        elBtnSave.addEventListener('click', this._saveEditPostBound);

        this._destroyEditPostBound = this._destroyEditPost.bind(this);
        elBtnCancel.addEventListener('click', this._destroyEditPostBound);

        this._html.container.append(elModal);
        
        this._html.edit = {};
        this._html.edit.form = elModal;
        this._html.edit.title = elTitle;
        this._html.edit.body = elBody;
        this._html.edit.btnCancel = elBtnCancel;
        this._html.edit.btnSave = elBtnSave;
    }

}

const twitter = new MyTwitter();
twitter.appendTo(document.body);