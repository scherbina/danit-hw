//
// Класс, объекты которого описывают параметры гамбургера. 
// 
// @constructor
// @param size        Размер
// @param stuffing    Начинка
// @throws {HamburgerException}  При неправильном использовании
//

function Hamburger(size, stuffing) {

    this._init = function() {
        this.toppingList = new Set;
    }

    this._setSize = function (size) {
        if (Hamburger.SIZES[size]) {
            this._size = size
        } else {
            // throw new HamburgerException('size_wrong', size)
            throw new HamburgerException('size_wrong', size, Hamburger.getPossibleValues('SIZES'))
        }
    };

    this._setStuffing = function (stuffing) {
        if (Hamburger.STUFFINGS[stuffing]) {
            this._stuffing = stuffing
        } else {
            // throw new HamburgerException('stuffing_wrong', stuffing)
            throw new HamburgerException('stuffing_wrong', stuffing, Hamburger.getPossibleValues('STUFFINGS'))
        }
    };

    if (arguments.length < 2) throw new HamburgerException('paramLessTwo', ...arguments);
    this._init();
    this._setSize(size);
    this._setStuffing(stuffing);
} 

// Размеры, виды начинок и добавок
Hamburger.SIZE_SMALL = 'SIZE_SMALL';
Hamburger.SIZE_LARGE = 'SIZE_LARGE';

Hamburger.SIZES = {
    [Hamburger.SIZE_SMALL]: {
        price: 50,
        calories: 20
    },
    [Hamburger.SIZE_LARGE]: {
        price: 100,
        calories: 40
    }
};
Hamburger.STUFFING_CHEESE = 'STUFFING_CHEESE';
Hamburger.STUFFING_SALAD = 'STUFFING_SALAD';
Hamburger.STUFFING_POTATO = 'STUFFING_POTATO';

Hamburger.STUFFINGS = {
    [Hamburger.STUFFING_CHEESE]: {
        price: 10,
        calories: 20
    },
    [Hamburger.STUFFING_SALAD]: {
        price: 20,
        calories: 5
    },
    [Hamburger.STUFFING_POTATO]: {
        price: 15,
        calories: 10
    }
};

Hamburger.TOPPING_MAYO = 'TOPPING_MAYO';
Hamburger.TOPPING_SPICE = 'TOPPING_SPICE';
Hamburger.TOPPINGS = {
    [Hamburger.TOPPING_MAYO]: {
        price: 20,
        calories: 5
    },
    [Hamburger.TOPPING_SPICE]: {
        price: 15,
        calories: 0
    }
};

Hamburger.getPossibleValues = function (list) {
    let listArr = [];
    for (key in Hamburger[list]) {
        listArr = [...listArr, key]
    }
    return listArr
}


//  Добавить добавку к гамбургеру. Можно добавить несколько
//  добавок, при условии, что они разные.
// 
//  @param topping     Тип добавки
//  @throws {HamburgerException}  При неправильном использовании

Hamburger.prototype.addTopping = function (topping) {
    if (arguments.length < 1) throw new HamburgerException('toppingEmpty');
    if (this.toppingList.has(topping)) throw new HamburgerException('toppingDuplicat', topping);
    
        if (Hamburger.TOPPINGS[topping]) {
            this.toppingList.add(topping)
        } else {
            throw new HamburgerException('topping_wrong', topping, Hamburger.getPossibleValues('TOPPINGS'))
        }
}


//  Убрать добавку, при условии, что она ранее была 
//  добавлена.
// 
//  @param topping   Тип добавки
//  @throws {HamburgerException}  При неправильном использовании
 
Hamburger.prototype.removeTopping = function (topping) {
    if (arguments.length < 1) throw new HamburgerException('toppingEmpty');
    if (!this.toppingList.has(topping)) throw new HamburgerException('noTopping', topping);
    
        if (Hamburger.TOPPINGS[topping]) {
            this.toppingList.delete(topping)
        } else {
            throw new HamburgerException('topping_wrong', topping, Hamburger.getPossibleValues('TOPPINGS'))
        }
}

//
// Получить список добавок.
//
// @return {Array} Массив добавленных добавок, содержит константы Hamburger.TOPPING_*
//
Hamburger.prototype.getToppings = function () {
    // let listArr = [];
    // this.toppingList.forEach(element => {
    //     listArr = [...listArr, element] 
    // });
    return [...this.toppingList]
}

//
// Узнать размер гамбургера
//
Hamburger.prototype.getSize = function () {
    return this._size
}

//
// Узнать начинку гамбургера
//
Hamburger.prototype.getStuffing = function () {
    return this._stuffing
}

//
// Узнать цену гамбургера
// @return {Number} Цена в тугриках
//
Hamburger.prototype.calculatePrice = function () {
    let price = 0;
    price += Hamburger.SIZES[this.getSize()].price;
    price += Hamburger.STUFFINGS[this.getStuffing()].price;
    this.getToppings().forEach(topping => price += Hamburger.TOPPINGS[topping].price)
    return  price
}

//
// Узнать калорийность
// @return {Number} Калорийность в калориях
//
Hamburger.prototype.calculateCalories = function () {
    let calories = 0;
    calories += Hamburger.SIZES[this.getSize()].calories;
    calories += Hamburger.STUFFINGS[this.getStuffing()].calories;
    this.getToppings().forEach(topping => calories += Hamburger.TOPPINGS[topping].calories)
    return calories;
}

//
// Представляет информацию об ошибке в ходе работы с гамбургером. 
// Подробности хранятся в свойстве message.
// @constructor 
//
function HamburgerException (error, ...params) {
    this.name = 'ОШИБКА';
    if (error === 'size_wrong') this.message = `Неверно указан размер [${params[0]}]. Возможные варианты: ${params.slice(1)}`
        else if (error === 'stuffing_wrong') this.message = `Неверно указана начинка [${params[0]}]. Возможные варианты: ${params.slice(1)}`
        else if (error === 'paramLessTwo') this.message = `Переданы параметры: [${params}]. Необходимо передать как минимум два параметра: размер и начинка`
        else if (error === 'toppingEmpty') this.message = `Не указано ни одной добавки`
        else if (error === 'toppingDuplicat') this.message = `Добавка [${params}] уже была добавлена ранее`
        else if (error === 'noTopping') this.message = `Добавка [${params}] не добавлена  - нечего удалять`
        else if (error === 'topping_wrong') this.message = `Неверно указана добавка [${params[0]}]. Возможные варианты: ${params.slice(1)}`
        else this.message = 'Возникла непредвиденная ошибка'
    
}

//Пример использования 

// маленький гамбургер с начинкой из сыра
let hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE);
// добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: ", hamburger.calculateCalories());
// сколько стоит
console.log("Price: ", hamburger.calculatePrice());
// я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит? 
console.log("Price with sauce: ", hamburger.calculatePrice());
// Проверить, большой ли гамбургер? 
console.log(`Is hamburger large: `, hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log(`Have toppings: ${hamburger.getToppings()}`); // 1

//Пример обработки ошибок не останавливая программу
try {
    hamburger2 = new Hamburger();
}
catch (e) {
    console.log(`${e.name}. ${e.message}`)
}

//Пример обработки ошибок с прерыванием выполнения
var h2 = new Hamburger(); // => HamburgerException: no size given
// передаем некорректные значения, добавку вместо размера
var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE); 
// => HamburgerException: invalid size 'TOPPING_SAUCE'

// добавляем много добавок
var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);
hamburger.addTopping(Hamburger.TOPPING_MAYO); 
// HamburgerException: duplicate topping 'TOPPING_MAYO'





