class IPDetection {
    constructor() {
        this._init();
    }

    _init() {
        this._data = {
            URL: {
                ip: 'https://api.ipify.org/?format=json',
                geo: 'http://ip-api.com/json/'
            },
            params: {
                geo: {
                    lang: 'ru',
                    fields: 'continent\,country\,regionName\,city\,district'
                }
            },
            lat2ru: {
                continent: 'Континент',
                country: 'Страна',
                regionName: 'Регион',
                city: 'Город',
                district: 'Район города'
            }
        };
        this._html = {};
        this._html.container = document.createElement('div');
        this._html.container.classList.add('IPDetection');

        this._html.btn = document.createElement('button');
        this._html.btn.innerText = 'Вычислить по IP'
        this._html.btn.classList.add('IPDetection__btn');
        this._html.container.append(this._html.btn);
        this._html.btn.addEventListener('click', this._detectIP.bind(this));
    }

    appendTo(parent) {
        parent.append(this._html.container)
    }

    async _detectIP() {
        const ipRespons = await fetch(this._data.URL.ip);
        const {ip} = await ipRespons.json();
        console.log(ip);
        let geoURL = new URL(this._data.URL.geo + ip);
        Object.keys(this._data.params.geo).forEach(key => geoURL.searchParams.append(key, this._data.params.geo[key]))
        const geoRespons = await fetch(geoURL);
        const geo = await geoRespons.json();
        const listEl = document.createElement('ul');
        this._html.container.append(listEl);
        

        Object.keys(geo).forEach((key) => {
            const listItem = document.createElement('li');
            listItem.innerText = `${this._data.lat2ru[key]}: ${geo[key]}`;
            listEl.append(listItem);
        })
        
    }
}

const ipdetect = new IPDetection();
ipdetect.appendTo(document.body);
