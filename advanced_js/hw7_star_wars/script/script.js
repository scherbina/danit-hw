class StarWars {
    constructor() {
        this._init();
        this._createUI();
        this._addListiner();
    }

    _init() {
        this._data = {};
        this._data.URL = 'https://swapi.dev/api/films/';
        this._html = {};
        this._html.container = document.createElement('div');
        this._html.container.classList.add('starwars');
        
    }

    _createUI() {
        this._html.title = document.createElement('h4');
        this._html.title.classList.add('starwars__title');
        this._html.title.innerText = 'StarWars films';
        this._html.container.append(this._html.title);

        this._html.btnLoad = document.createElement('button');
        this._html.btnLoad.classList.add('starwars__btnLoad');
        this._html.btnLoad.innerText = 'Load...';
        this._html.container.append(this._html.btnLoad);
    }

    _renderFilms() {
        this._html.films = document.createElement('ul');
        this._html.films.classList.add('starwars__films');
        this._html.container.append(this._html.films);
    }

    _renderFilm(film, id) {
        const filmEl = document.createElement('li');
        filmEl.classList.add('starwars__film');
        const filmTitleEl = document.createElement('h4');
        const filmCrawlEl = document.createElement('p');
        filmTitleEl.innerText = `Episod ${film.episode_id}. ${film.title}`;
        filmCrawlEl.innerText = film.opening_crawl;
        filmEl.append(filmTitleEl);
        filmEl.append(filmCrawlEl);
        this._html.films.append(filmEl);

        const charactersEl = document.createElement('ul');
        charactersEl.classList.add('starwars__characters');
        charactersEl.innerText = 'Characters:';
        filmEl.append(charactersEl);

        this._html.films[id] = {
            film: filmEl, 
            filmTitle: filmTitleEl, 
            filmCrawl: filmCrawlEl, 
            characters: charactersEl
        };
    }

    _renderCharacter(character, filmID) {
        const characterEl = document.createElement('li');
        characterEl.classList.add('starwars__character');
        characterEl.innerText = character;
        this._html.films[filmID].characters.append(characterEl);
    }

    _clearFilms() {
        if (this._html.films) this._html.films.remove();
    }

    _renderSpinner(id) {
        const spinEl = document.createElement('div');
        spinEl.classList.add('lds-ellipsis');
        spinEl.innerHTML = '<div></div><div></div><div></div><div></div>';
        this._html.films[id].spinner = spinEl;
        this._html.films[id].filmTitle.after(spinEl);
    }

    _renderInlineSpinner(element) {
        const spinEl = document.createElement('i');
        spinEl.classList.add('c-inline-spinner');
        element.append(spinEl);
        return spinEl
    }

    _removeSpinner(spinner) {
        spinner.remove();
    }

    _addListiner() {
        this._loadFilmsBound = this._loadFilms.bind(this);
        this._html.btnLoad.addEventListener('click', this._loadFilmsBound);
    }

    _loadFilms() {
        this._html.btnLoad.disabled = true;
        this._clearFilms();
        this._html.spinner = this._renderInlineSpinner(this._html.btnLoad);
        fetch(this._data.URL)
            .then(respons => respons.json())
            .then(films => {
                this._data.films = films.results;
                this._renderFilms();
                
                let promiseLoadFilmsCharacters = [];
                
                
                this._data.films.forEach((film, id) => {
                    this._renderFilm(film, id);
                    this._html.films[id].spinner = this._renderInlineSpinner(this._html.films[id].filmTitle);
                    this._data.films[id].loadedCharacters = [];
                    
                    const promiseLoadFilmCharacters = this._data.films[id].characters.map(characterURL => fetch(characterURL).then(respons => respons.json()));
                    promiseLoadFilmsCharacters = [...promiseLoadFilmsCharacters, ...promiseLoadFilmCharacters];
                    Promise.all(promiseLoadFilmCharacters)
                        .then(characters => {
                            this._removeSpinner(this._html.films[id].spinner);
                            this._data.films[id].loadedCharacters = characters;
                            this._data.films[id].loadedCharacters.forEach(character => {
                                this._renderCharacter(character.name, id);
                            })
                        })
                        
                    
                })
                Promise.all(promiseLoadFilmsCharacters)
                    .then(() => {
                        this._removeSpinner(this._html.spinner);
                        this._html.btnLoad.disabled = false;
                    })
            })
        
    }

    appendTo(parent) {
        parent.append(this._html.container)
    }

}

const sw = new StarWars();
sw.appendTo(document.body);
