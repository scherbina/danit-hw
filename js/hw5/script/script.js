function isDateStr (date) {
    if (date && typeof date === 'string') {
        const tmpArr = date.split('.');
        const tmpDate = new Date(+tmpArr[2], +tmpArr[1] - 1, +tmpArr[0]);
        return (tmpDate.getFullYear() === +tmpArr[2] && tmpDate.getMonth() === +tmpArr[1]-1 && tmpDate.getDate() === +tmpArr[0])
    }
    return false;
}

function createNewUser() {
    let inFName = inLName = inBirthDate = '';

    let newUser = {
        
        getLogin() {
            if (!!this.firstName && !!this.lastName) {
                return (this.firstName[0]+this.lastName).toLowerCase()
            } else {
                return 'ERROR'
            }
        },

        getFullName() {
            return (`${this.firstName} ${this.lastName}`)
        },

        setFirstName(newFName) {
            Object.defineProperty(this, 'firstName', {writable: true, configurable: true});
            this.firstName = newFName;
            Object.defineProperty(this, 'firstName', {writable: false, configurable: true});
        },

        setLastName(newLName) {
            Object.defineProperty(this, 'lastName', {writable: true, configurable: true});
            this.lastName = newLName;
            Object.defineProperty(this, 'lastName', {writable: false, configurable: true});
        },

        getYearBirth() {
            return this.birthday.slice(-4);
        },
        getMonthBirth() {
            return this.birthday.slice(3,5);
        },
        getDayBirth() {
            return this.birthday.slice(0,2);
        },

        getAge() {
            const today = new Date();
            let age = today.getFullYear() - +this.getYearBirth();
            if (today.getMonth()+1 < +this.getMonthBirth() ||
                (today.getMonth()+1 === +this.getMonthBirth() && today.getDate() < +this.getDayBirth())
            ) age--
            return age
        },

        getPassword() {
            return this.firstName[0] + this.lastName[0].toLowerCase() + this.lastName.slice(1) + this.getYearBirth()
        }
    };



    do {
        inFName = prompt('Введите имя: ', inFName || '');
        if (!inFName) continue;
        inFName = inFName.trim();
        inFName = inFName[0].toUpperCase() + inFName.slice(1);
    } while (!inFName || inFName.search(/\d|\s/g) !== -1);    
    
    do {
        inLName = prompt('Введите фамилию: ', inLName || '');
        if (!inLName) continue;
        inLName = inLName.trim();
        inLName = inLName[0].toUpperCase() + inLName.slice(1);
    } while (!inLName || inLName.search(/\d|\s/g) !== -1);    
    
    do {
        inBirthDate = prompt('Введите дату рождения (в формате ДД.ММ.ГГГГ): ', inBirthDate || '');
    } while (!isDateStr(inBirthDate));    

    newUser.setFirstName(inFName);
    newUser.setLastName(inLName);
    newUser.birthday = inBirthDate;

    return newUser;
}


let newUser = createNewUser();

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());

