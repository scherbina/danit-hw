let inNum1 = inNum2 = inOp = '';

function calc(num1, num2, op) {
    switch (op) {
        case '+':
            return num1 + num2;
        case '-':
            return num1 - num2;
        case '*':
            return num1 * num2;
        case '/':
            return num1 / num2;
        default:
            return 'Operation unknown'
    }
};

do {
    inNum1 = prompt('Enter number 1: ', inNum1 || '');
} while (inNum1 === null || inNum1.trim() === '' || isNaN(+inNum1));

do {
    inNum2 = prompt('Enter number 2: ', inNum2 || '');
} while (inNum2 === null || inNum2.trim() === '' || isNaN(+inNum2));

do {
    inOp = prompt('Enter operation (+ - * /): ', inOp || '');
} while (inOp !== '+' && inOp !== '-' && inOp !== '*' && inOp !== '/');

console.log (`${inNum1} ${inOp} ${inNum2} = ${calc(+inNum1, +inNum2, inOp)}`);
