function initApp (root) {
    const app = document.querySelector(root)
    initCalculator(app);
}

function initCalculator(app) {
    let calc = {
        result: 0,
        prevResult: 0,
        operation: '=',
        isNewNumber: true,
        memory: 0,
        isMRCPressed: false,
    }

    const display = app.querySelector('.display input');
    display.value = '0';

    const numpad = app.querySelector('.keys');
    numpad.addEventListener('click', calculator);
    document.addEventListener('keypress', calculator);
    
    function calculator (e) {
        let keyClicked;
        
        if (e.type === 'click' && e.target.tagName === 'INPUT') keyClicked = e.target.value;
        if (e.type === 'keypress' && '1234567890.+-*/='.includes(e.key)) keyClicked = e.key;
        if (e.type === 'keypress' && e.key === 'Enter') keyClicked = '=';

        if (keyClicked) {
            //Взять число из памяти
            if (keyClicked === 'mrc') {
                if (calc.isMRCPressed) {
                    calc.isMRCPressed = false;
                    calc.memory = 0;
                    app.querySelector('.displayFlag').style.display = '';
                } else {
                    calc.result = calc.memory;
                    display.value = calc.result;
                    calc.isMRCPressed = true;
                }
                calc.isNewNumber = true;
                return;
            }

            calc.isMRCPressed = false;

            //Добавить в память
            if (keyClicked === 'm+') {
                calc.memory += calc.result;
                app.querySelector('.displayFlag').style.display = 'inline-block';
                return;
            }

            //Вычесть из памяти
            if (keyClicked === 'm-') {
                calc.memory -= calc.result;
                app.querySelector('.displayFlag').style.display = 'inline-block';
                return;
            }

            //очистить калькулятор
            if (keyClicked === 'C') {
                calc.result = 0;
                display.value = '0';
                calc.operation = '=';
                calc.isNewNumber = true;
                return;
            }

            //Выполнить операции
            if ('+-*/='.includes(keyClicked)) {
                if (calc.operation === '+') calc.result = calc.prevResult + calc.result;
                if (calc.operation === '-') calc.result = calc.prevResult - calc.result;
                if (calc.operation === '/') calc.result = calc.prevResult / calc.result;
                if (calc.operation === '*') calc.result = calc.prevResult * calc.result;
                
                if (keyClicked === '=') {
                    calc.prevResult = 0;
                    display.value = calc.result;
                } else {
                    calc.prevResult = +calc.result;
                    if ('+-*/'.includes(calc.operation)) display.value = calc.result;
                }
                calc.isNewNumber = true;
                calc.operation = keyClicked;
                return;
            }

            //обнулить диспелей если вводится новое число
            if (calc.isNewNumber) {
                calc.result = 0;
                display.value = '0';
            }
            
            //ставим флаг не нового числа (т.е. вводим разряды одного и того же числа)
            calc.isNewNumber = false;
            
            //Проверка на вторую точку в числе
            if (keyClicked === '.' && display.value.indexOf('.') !== -1) return;

            //убрать дубликаты первых нулей
            if (display.value === '0' && '1234567890'.includes(keyClicked)) display.value = '';

            //обновить дисплей и результат новым числом
            display.value += keyClicked;
            calc.result = +display.value;
            
        }
    };
}

initApp('.box');