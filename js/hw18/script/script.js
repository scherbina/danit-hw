//Исходный объект
let obj1 = {
    propStr: 'String',
    propNum: 123,
    propArr: [4,7,9],
    propArrObj: [
        {
            name: 'name1',
            descr: 'Description1'
        },
        {
            name: 'name2',
            descr: 'Description2'
        },
        {
            name: 'name3',
            descr: 'Description3'
        },
        {
            name: 'name4',
            descr: 'Description4'
        }
    ],
    propObj: {
        fName: 'FirstName',
        lName: 'LastName',
        contact: {
            phone: '+34252222',
            email: 'abc@ggdfg.cft',
            propArr: ['A0','JS','HT'],
            propNull: null
        },
        age: 23
    },
    propBool: true,
    propNull: null
}

//Функция копирования
function copyObj(trgObj, srcObj) {
    for (let prop in srcObj) {
        if (typeof srcObj[prop] === 'object') {
            if (srcObj[prop] === null) {
                trgObj[prop] = null
            } else {
                for (key in srcObj[prop]) {
                    trgObj[prop] = copyObj({},srcObj[prop])    
                }
            }
        } else trgObj[prop] = srcObj[prop]
    }
    return trgObj;
}

//Проверка
console.log(obj1); //Исходный объект
let obj2 = copyObj({}, obj1);
console.log(obj2); //Копия исходного объекта

//Мутируем исходный объект
obj1.propStr = 'StringNew';
obj1.propArr.push(1);
obj1.propObj.fName = 'FirstNameNew';
obj1.propObj.contact.email = 'newemail@';
obj1.propObj.contact.propArr.push('NewElement');
obj1.propObj.contact.propNull = '';
obj1.propArrObj[1].name = 'Name new';
obj1.propNull = 1;

//Проверяем, что исходный изменился, а копия осталась прежней
console.log(obj1);
console.log(obj2);

