const slider = document.querySelector('.slider');
slider.addEventListener('click', slideShow);

function slideShow (e) {
        if (e.target.tagName === 'BUTTON') {
            const curImage = slider.querySelector('.active');
            const nextImage = getNextImage(curImage, e.target.dataset.direction === "true" ? true : false);
            changeImage(curImage, nextImage);
        }
}

function getNextImage(curImage, directionNext) {
    let nextImage;

    if (directionNext) {
        if (!curImage.nextElementSibling) {
            curImage.parentElement.prepend(curImage);
            renewSlider(curImage)
        }
        nextImage = curImage.nextElementSibling;
    } else {
        if (!curImage.previousElementSibling) {
            curImage.parentElement.append(curImage);
            renewSlider(curImage)
        }
        nextImage = curImage.previousElementSibling;
    }
    return nextImage;
}

function changeImage(curImage, nextImage) {
    curImage.classList.remove('active');
    curImage.parentElement.style.transform = `translateX(${-nextImage.offsetLeft + 10}px)`;
    nextImage.classList.add('active');
}

function renewSlider(curImage) {
    curImage.parentElement.classList.add('noTransition');
    curImage.parentElement.style.transform = `translateX(${-curImage.offsetLeft + 10}px)`;
    curImage.parentElement.offsetHeight; //триггер(костыль) - заставляет браузер обновить контент
    curImage.parentElement.classList.remove('noTransition');
}


