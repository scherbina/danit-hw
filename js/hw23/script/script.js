function initApp (root) {
    const app = document.querySelector(root)
    gameCore(app);
}

function gameCore(app) {
    const game = {

        app,                //элемент HTML (контейнер), в котором будет располагаться вся игра
        btnRestart: null,   //элемент HTML, кнопка рестарта игры
        elScore: null,      //элемент HTML, очки игры
        table: null,        //элемент HTML, игровое поле

        rows: 8,            //размер строк игрового поля (по умолчанию)
        cols: 8,            //размер столбцов игрового поля (по умолчанию)
        minesCnt () {       //подсчет количества мин в зависимости от размеров поля
            return Math.floor(this.rows * this.cols / 6)
        },
        mines: [],          //массив игрового поля с расставленными наугад минами
        cellsChecked: [],   //массив игрового поля с уже проверенными ячейками

        minesDetected() {   //храним количество обнаруженных мин (ячеек, помеченных как мина)
            return this.table.querySelectorAll('.bomb').length;
        },   
        checkedCnt () {     //количство уже проверенных ячеек (помеченных как пустые)
            return this.table.querySelectorAll('.empty').length
        },

        isGameStarted: false, //флаг запущенной игры

        //Обнуление параметров и запуск новой игры
        initGame () {
            this.mines = [];
            this.cellsChecked = [];
            this.isGameStarted = false;

            this.createNewField();
            this.createParamForm();
            this.initBombField();
            this.startNewGame();
        },

        //Создать HTML разметку игрового поля
        createNewField() {
            this.btnRestart = document.createElement('button');
            this.btnRestart.classList.add('btn');
            this.btnRestart.innerText = 'Начать игру заново';
            this.btnRestart.style.visibility = 'hidden';
            this.app.append(this.btnRestart);
            
            this.elScore = document.createElement('p');
            this.elScore.classList.add('score');
            this.elScore.innerText = `Bomb detected: 0 / ${this.minesCnt()}`;
            this.app.append(this.elScore);

            this.table = document.createElement('table');
            this.table.classList.add('table');
            this.app.append(this.table);

            for (let i = 0; i < this.rows; i++) {
                this.mines[i] = [];
                this.cellsChecked[i] = [];
                let tr = document.createElement('tr');
                this.table.append(tr);
                for (let j = 0; j < this.cols; j++) {
                    this.mines[i][j] = false;
                    this.cellsChecked[i][j] = false;
                    let td = document.createElement('td');
                    td.classList.add('td');
                    tr.append(td);
                }
            };
        }, 

        //Создать HTML разметку блока параметров
        createParamForm() {
            const elParam = document.createElement('form');
            elParam.classList.add('formParams');
            
            const elParamSet = document.createElement('fieldset');
            elParam.append(elParamSet);
            
            elParamSet.append(document.createElement('legend'));
            elParamSet.firstElementChild.textContent = 'Field Size';


            elParamLabel = document.createElement('label');
            elParamLabel.classList.add('paramLabel');
            elParamLabel.textContent = 'Rows: '
            elParamSet.append(elParamLabel);

            const elParamRowsInput = document.createElement('input');
            elParamRowsInput.classList.add('paramInput');
            elParamRowsInput.value = game.rows;
            elParamLabel.append(elParamRowsInput);

            elParamLabel = document.createElement('label');
            elParamLabel.classList.add('paramLabel');
            elParamLabel.textContent = 'Cols: '
            elParamSet.append(elParamLabel);

            const elParamColsInput = document.createElement('input');
            elParamColsInput.classList.add('paramInput');
            elParamColsInput.value = game.cols;
            elParamLabel.append(elParamColsInput);

            const elParamBtn = document.createElement('button');
            elParamBtn.classList.add('btn');
            elParamBtn.textContent = 'Resize';
            elParam.append(elParamBtn);

            elParamBtn.addEventListener('click', eventListinerResize);
            this.app.append(elParam);
        }, 

        //Расставить бомбы в случайном порядке
        initBombField() {
            let mineRow, mineCol;
            let i = 0;
            do  {
                mineRow = Math.floor(Math.random()*this.rows);
                mineCol = Math.floor(Math.random()*this.cols);
                if (!this.mines[mineRow][mineCol]) {
                    this.mines[mineRow][mineCol] = true;
                    i++
                }
            } while (i < this.minesCnt())
        },

        //Показать кнопку перезапуска игры
        showRestartButton () {
            this.btnRestart.style.visibility = 'visible';
            this.btnRestart.addEventListener('click', eventListinerRestartGame);
        },

        //Начать новую игру с измененными размерами игрового поля
        resize(e) {
            e.preventDefault();
            const inputRow = this.app.querySelectorAll('.paramInput')[0];
            const inputCol = this.app.querySelectorAll('.paramInput')[1];
            if (isInputFieldSizeCorrect(inputRow) && isInputFieldSizeCorrect(inputCol)) {
                this.rows = +inputRow.value;
                this.cols = +inputCol.value;
                this.restartGame();
            }
        },        
        
        //Запустить игру
        startNewGame () {
            this.table.addEventListener('click', eventListinerPlayerTurn);
            this.table.addEventListener('contextmenu', eventListinerPlayerTurn);
            this.table.addEventListener('dblclick', eventListinerPlayerTurn);
        },

        //Проверка хода игрока (клик на игровом поле)
        playerTurn (e) {
            e.preventDefault();
            if (!this.isGameStarted) {
                this.isGameStarted = true;
                this.showRestartButton();
            } 
            
            if (e.target.tagName === 'TD') {
                const clickedRow = [...this.table.children].indexOf(e.target.parentElement);
                const clickedCol = [...e.target.parentElement.children].indexOf(e.target);
    
                if (e.type === 'dblclick') {
                    if (e.target.textContent !== '' && e.target.classList.contains('empty')) {
                        if (+e.target.textContent === this.checkCountDetectedSiblingBomb(clickedRow, clickedCol)) {
                            this.openSiblingCell(clickedRow, clickedCol);
                        }
                    }
                    
                }

                if (e.which === 1 && !e.target.classList.contains('bomb')) {
                    if (this.mines[clickedRow][clickedCol]) {
                        this.boom(clickedRow, clickedCol)
                    } else {
                        this.checkSiblingCell(clickedRow, clickedCol);
                    }
                }
                if (e.which === 3 && !e.target.classList.contains('empty')) {
                    e.target.classList.toggle('bomb');
                    this.elScore.textContent = `Bomb detected: ${this.minesDetected()} / ${this.minesCnt()}`;
                    e.target.innerText === '?' ? e.target.innerText = '' : e.target.innerText = '?';
                }
                
                if (this.isWin()) this.endGame(true);
            }
        }, 

        //Проверка количества спрятанных бомб рядом с нашей ячейкой
        checkSiblingCell (row, col) {
            if (this.mines[row][col]) {
                this.boom(row, col)
                return;
            }

            const element = this.table.children[row].children[col];
            
            this.cellsChecked[row][col] = true;
            if (!element.classList.contains('bomb')) element.classList.add('empty');

            if (this.isWin()) return
    
            let countBomb = 0;
            const fromRow = (row - 1 < 0) ? 0 : row - 1;
            const toRow = (row + 1 === this.rows) ? this.rows - 1 : row + 1;
            const fromCol = (col - 1 < 0) ? 0 : col - 1;
            const toCol = (col + 1 === this.cols) ? this.cols - 1 : col + 1;
            for (let i = fromRow; i <= toRow; i++) {
                for (let j = fromCol; j <= toCol; j++) {
                    if (this.mines[i][j]) countBomb++ 
                }
            }
            if (countBomb !== 0) {
                if (!element.classList.contains('bomb')) element.innerText = countBomb;
                return
            } else {
                element.textContent = '';
                for (let i = fromRow; i <= toRow; i++) {
                    for (let j = fromCol; j <= toCol; j++) {
                        if (this.cellsChecked[i][j]) continue;
                        this.checkSiblingCell(i, j);
                    }
                }
            }
        }, 

        //Проверка количесва уже найденных бомб рядом с нашей ячейкой
        checkCountDetectedSiblingBomb (row, col) {
            let countBomb = 0;
            const fromRow = (row - 1 < 0) ? 0 : row - 1;
            const toRow = (row + 1 === this.rows) ? this.rows - 1 : row + 1;
            const fromCol = (col - 1 < 0) ? 0 : col - 1;
            const toCol = (col + 1 === this.cols) ? this.cols - 1 : col + 1;
            for (let i = fromRow; i <= toRow; i++) {
                for (let j = fromCol; j <= toCol; j++) {
                    if (this.table.children[i].children[j].classList.contains('bomb')) countBomb++ 
                }
            }
            return countBomb
        },

        //Открыть соседние ячейки если цифра текущей ячейки совпадает с количеством найденных бомб
        openSiblingCell (row, col) {
            const fromRow = (row - 1 < 0) ? 0 : row - 1;
            const toRow = (row + 1 === this.rows) ? this.rows - 1 : row + 1;
            const fromCol = (col - 1 < 0) ? 0 : col - 1;
            const toCol = (col + 1 === this.cols) ? this.cols - 1 : col + 1;
            for (let i = fromRow; i <= toRow; i++) {
                for (let j = fromCol; j <= toCol; j++) {
                    if (!this.table.children[i].children[j].classList.contains('bomb') && !this.table.children[i].children[j].classList.contains('empty')) {
                        this.checkSiblingCell(i, j)
                    } 
                }
            }
        },

        //Попали на бомбу
        boom(row, col) {
            this.table.children[row].children[col].classList.add('boom');
            this.showBombField();
            this.endGame(false);
        },

        //Проверка открыты ли все ячейки и найдены все бомбы
        isWin () {
            if (this.checkedCnt() === this.rows * this.cols - this.minesCnt() && this.minesDetected() === this.minesCnt()) {
                return true;
            } else {
                return false
            }
        },
        
        //Показать сообщение "YOU WIN!!!" и закончить игру
        endGame (isWin) {
            this.table.removeEventListener('click', eventListinerPlayerTurn);
            this.table.removeEventListener('contextmenu', eventListinerPlayerTurn);
            this.table.removeEventListener('dblclick', eventListinerPlayerTurn);
    
            elWin = document.createElement('p');
            if (isWin) {
                elWin.classList.add('endGameMsg');
                elWin.textContent = 'YOU WIN!!!';
            } else {
                elWin.classList.add('endGameMsg');
                elWin.textContent = 'YOU LOOS!!!';
            }
            elWin.style.width = this.table.offsetWidth + 'px';
            this.app.append(elWin);
            elWin.style.top = this.table.offsetTop + this.table.offsetHeight / 2 -  elWin.offsetHeight / 2 + 'px'

            this.table.removeEventListener('click', eventListinerPlayerTurn);
            this.table.removeEventListener('contextmenu', eventListinerPlayerTurn);
            this.table.removeEventListener('dblclick', eventListinerPlayerTurn);
        }, 

        //Начать игру заново
        restartGame () {
            this.table.removeEventListener('click', eventListinerPlayerTurn);
            this.table.removeEventListener('contextmenu', eventListinerPlayerTurn);
            this.table.removeEventListener('dblclick', eventListinerPlayerTurn);
            this.btnRestart.removeEventListener('click', eventListinerRestartGame);
    
            this.app.innerHTML = '';
            this.initGame();
        }, 

        //Показать все бомбы
        showBombField () {
            this.mines.forEach((row,i) => {
                row.forEach((bomb,j) => {
                    if (bomb) {
                        this.table.children[i].children[j].classList.add('bomb');
                        this.table.children[i].children[j].textContent = 'BOMB'
                    }
                })
            })
        },
    };
    
    function eventListinerPlayerTurn(e) {
        game.playerTurn(e)
    }

    function eventListinerRestartGame() {
        game.restartGame()
    }

    function eventListinerResize(e) {
        game.resize(e)
    }

    function isInputFieldSizeCorrect(input) {
        if (!input.value || isNaN(input.value) || +input.value < 0 || +input.value % 1 !== 0) {
            input.classList.add('inputError');
            return false
        } else return true
    }
    
    game.initGame();
}

initApp('.mines');

