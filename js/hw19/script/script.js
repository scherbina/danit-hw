//массив из чисел, который представляет скорость работы разных членов команды. 
//Количество элементов в массиве означает количество человек в команде. 
//Каждый элемент означает сколько стори поинтов (условная оценка сложности выполнения задачи) может выполнить данный разработчик за 1 день.
const teamMemberSpeedPerDay = [5, 3, 10, 15, 11, 8];

//массив из чисел, который представляет собой беклог (список всех задач, которые необходимо выполнить). 
//Количество элементов в массиве означает количество задач в беклоге. 
//Каждое число в массиве означает количество стори поинтов, необходимых для выполнения данной задачи.
const backlog = [2, 5, 3, 1, 8, 11, 23, 55, 123, 13, 45, 23, 78, 4, 1, 3, 2, 56, 23, 19, 10, 1];

//дата дедлайна (объект типа Date).
const deadLineStr = '23.03.2020';

// Работа идет по 8 часов в день по будним дням
const workTimePerDay = 8;
const workDays = [1, 2, 3, 4, 5];

// curHourAllow. true - учитывать в рассчетах работу за текущий час или false - отсчет вести со следующего часа.
// если true, то текущий час в работе учитывается только если текущие минуты равны 00
const curHourAllow = true;

// После выполнения, функция должна посчитать, успеет ли команда разработчиков выполнить все задачи из беклога до наступления дедлайна
// (работа ведется начиная с сегодняшнего дня).
// Если да, вывести на экран сообщение: Все задачи будут успешно выполнены за ? дней до наступления дедлайна!. Подставить нужное число дней в текст.
// Если нет, вывести сообщение Команде разработчиков придется потратить дополнительно ? часов после дедлайна, чтобы выполнить все задачи в беклоге



function evaluateTeamTime(teamMemberSpeedPerDay, backlog, deadLineStr) {

    const deadLineDate = new Date(deadLineStr.split('.')[2], deadLineStr.split('.')[1]-1, deadLineStr.split('.')[0], 23, 59, 59, 999);
    const today = new Date();

    //Считаем общее кол-во стори поинтов для всех задач
    let sumBacklogPoints = 0;
    for (point of backlog) sumBacklogPoints += point;

    //Считаем производительность всей комманды стори поинтов за один день
    let teamSpeedPerDay = 0;
    for (speed of teamMemberSpeedPerDay) teamSpeedPerDay += speed;

    //Считаем производительность всей комманды стори поинтов 1 час
    const teamSpeedPerHour = teamSpeedPerDay / workTimePerDay;

    //Считаем за сколько часов комманда сможет выполнить все задачи
    const sumTeamWorkHours = Math.ceil(sumBacklogPoints / teamSpeedPerHour); 

    //Если сегодня рабочий день, то считаем сколько осталось полных рабочих часов до конца суток
    let workHoursTodayRemain = 0;
    if (workDays.includes(today.getDay())) {
        if (curHourAllow && today.getMinutes() === 0) {
            workHoursTodayRemain = 24 - today.getHours()
        } else {
            workHoursTodayRemain = 24 - today.getHours() - 1
        }
    }

    //Сколько осталось проработать часов в другие дни (кроме сегодня)
    const workHoursRemain = sumTeamWorkHours - workHoursTodayRemain;

    //Сколько осталось проработать полных дней (кроме сегодня)
    const workDaysRemain = Math.floor(workHoursRemain / 8);

    //Сколько останется проработать часов в последний день
    const workHoursLastDayRemain = workHoursRemain - workDaysRemain * 8;

    //Узнаем дату и время завершения работы
    let endDate = today;
    let day =  dayWork = workDaysUntilDeadLine = 0;
    while (dayWork <= workDaysRemain) {
        day++;
        let tomorrow = new Date(today);
        tomorrow.setDate(tomorrow.getDate() + day);
        if (workDays.includes(tomorrow.getDay())) {
            endDate = tomorrow;
            if (endDate <= deadLineDate) workDaysUntilDeadLine++
            dayWork++
        }
    }
    
    if (endDate < deadLineDate) {
        const daysBefore = Math.floor((deadLineDate - endDate) / (1000 * 60 * 60 * 24));
        if (endDate.getDate() === deadLineDate.getDate()) {
            console.log(`Все задачи будут успешно выполнены в день дедлайна! В день дедлайна комменда проработает ${workHoursLastDayRemain} часов`)    
        } else {
            console.log(`Все задачи будут успешно выполнены за ${daysBefore} дней до наступления дедлайна!`)
        }
        
    } else {
        const hoursAfter = sumTeamWorkHours - workHoursTodayRemain - workDaysUntilDeadLine * 8;
        console.log(`Команде разработчиков придется потратить дополнительно ${hoursAfter} часов после дедлайна, чтобы выполнить все задачи в беклоге`);
    }
}

evaluateTeamTime(teamMemberSpeedPerDay, backlog, deadLineStr);


