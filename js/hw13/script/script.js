function setTheme () {
    let style = document.createElement('link');
    style.rel = 'stylesheet';
    style.href = `./css/style${localStorage.getItem('hw13theme')}.css`;
    document.head.append(style);
}

if (!localStorage.getItem('hw13theme'))  localStorage.setItem('hw13theme', '1');

let btnTheme = document.getElementById('themes')

btnTheme.addEventListener('click', () => {
    localStorage.getItem('hw13theme') === '1' ? localStorage.setItem('hw13theme', '2') : localStorage.setItem('hw13theme', '1');
    setTheme()
    setTimeout(() => document.head.querySelector('link[href^="."').remove(), 0);
});

setTheme();
