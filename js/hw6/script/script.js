const arr = [1, 2, '3', '4', 5, '6', {'1': 1, '2': 2}, true, [0, 9, 8], null];

function filterBy (arr, type) {
    let resultArr = [];
    arr.forEach(function(item, i, arr) {
        if (typeof item !== type) {
            if (type !== null || item !== null) resultArr.push(item)
        };  
        if (type === 'object' && item === null) resultArr.push(item);
    });
    return resultArr;
}

console.log('Заданный массив:');
console.log(arr);
console.log('Фильтрация строк:');
console.log(filterBy(arr, 'string'));
console.log('Фильтрация чисел:');
console.log(filterBy(arr, 'number'));
console.log('Фильтрация null:');
console.log(filterBy(arr, null));
console.log('Фильтрация объектов:');
console.log(filterBy(arr, 'object'));
console.log('Фильтрация логических:');
console.log(filterBy(arr, 'boolean'));