
const countX = 10;
const countY = 10;

let btnDrawCircle = document.getElementById('btnDrawCircle');

let form = document.createElement('form');
form.classList.add('formDrawCircle');
form.innerHTML = `
        <input id="diam" type="text" placeholder="Диаметр круга в px">
        <button id="btnDraw" class="btn">Нарисовать</button>
`;
document.body.append(form);


btnDrawCircle.addEventListener('click', (e) => {
    e.preventDefault();
    let btnDraw = document.getElementById('btnDraw');
    let input = document.getElementById('diam');
    input.style.display = 'block';
    btnDraw.style.display = 'block';
    btnDraw.addEventListener('click', (e) => {
        e.preventDefault();
        
        diam = input.value;
        if (!diam || isNaN(diam) || +diam <= 0) {
            input.classList.remove('diamOK')
            input.classList.add('diamEr')
            return;
        } else {
            input.classList.remove('diamEr')
            input.classList.add('diamOK')
        }

        let circlesBlock = document.querySelector('.circles');
        if (!circlesBlock) {
            circlesBlock = document.createElement('div');
            circlesBlock.classList.add('circles');
            document.body.append(circlesBlock);
        } else circlesBlock.innerHTML = '';

        circlesBlock.style.width = `${+diam * countX}px`;
        circlesBlock.style.height = `${+diam * countY}px`;

        for (let i = 1; i <= countX * countY; i++) {
            let canvas = document.createElement('canvas');
            canvas.height = diam;
            canvas.width = diam;
            let context = canvas.getContext('2d');
            context.arc(diam / 2, diam / 2, diam / 2, 0, Math.PI * 2, false);
            context.fillStyle = `rgb(${Math.random()*256}, ${Math.random()*256}, ${Math.random()*256})`;
            context.fill()
            circlesBlock.append(canvas);
        };

        circlesBlock.addEventListener('click', (e) => {
            if (e.target.tagName === 'CANVAS') e.target.remove();
        })
    });
});
