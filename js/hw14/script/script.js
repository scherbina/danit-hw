$(document).ready(function(){
    $('.navbar.inner a').click(function(e){
        e.preventDefault();
        const anchor = $(document).find(`a[name$=${$(this).attr('href').slice(1)}]`)[0];
        $('html, body').animate({
            scrollTop: $(anchor).offset().top
        }, 1000);
        
    });

    $('.btnHideSection').click(function(){
        $(this).prev().slideToggle('fast');
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $(this).text('Show')
        } else {
            $(this).text('Hide')

            const elSection = $(this).prev()[0];
            setTimeout( function() {
                $('html, body').scrollTop(elSection.offsetTop);
            }, 100);
        }
    });

    $(document).scroll(function() {
        
        if ($('html, body').scrollTop() > $('html')[0].clientHeight) {
            $('.slideTop').css('opacity', '1');
        } else {
            $('.slideTop').css('opacity', '0');
        }
    })

    $('.slideTop').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    })

});