const vehicles = [
    {
        name: 'TOYOTA',
        description: 'Some text about brand Toyota',
        contentType: {
            name: 'Toyota corporation content',
            description: 'Some description abou TOYOTA corp content'
        },
        locales: [
            {
                name: 'en_US',
                description: 'English USA'
            },
            {
                name: 'ru_RU',
                description: 'Russian Russia'
            },
            {
                name: 'ua_UA',
                description: 'Ukrainian, Ukraine'
            }
        ]
    },
    {
        name: 'MERCEDES',
        description: 'Some text about brand Mercedes',
        contentType: {
            name: 'Mercedes corporation content',
            description: 'Some description about MERCEDES corp content'
        },
        locales: [
            {
                name: 'en_UK',
                description: 'English United Kingdom'
            },
            {
                name: 'ru_RU',
                description: 'Russian Russia'
            },
            {
                name: 'ua_RU',
                description: 'Ukrainian, Russia'
            }
        ]
    },
    {
        name: 'VOLKSWAGEN',
        description: 'Some text about brand Volkswagen',
        contentType: {
            name: 'Volkswagen corporation content',
            description: 'Some description abou VOLKSWAGEN corp content'
        },
        locales: [
            {
                name: 'de_GE',
                description: 'deutch Germany'
            },
            {
                name: 'en_US',
                description: 'English USA'
            },
            {
                name: 'hu_HU',
                description: 'Hungrian Hungria'
            },
            {
                name: 'ua_UA',
                description: 'Ukrainian, Ukraine'
            },
            
        ]
    },
    {
        name: 'ZAZ',
        description: 'Some text about brand ZAZ',
        contentType: {
            name: 'ZAZ corporation content',
            description: 'Some description abou ZAZ corp content'
        },
        locales: [
            {
                name: 'ua_RU',
                description: 'Ukrainian, Russia'
            },
            {
                name: 'ua_UA',
                description: 'Ukrainian, Ukraine'
            },
            {
                name: 'ru_RU',
                description: 'Russian Russia',
                uniq: 'test'
            }
        ]
    }
]

function getProp(obj, path, keyWords, fields) {
    let itemPath;
    // let result = false;

    for (let prop in obj) {
        if (typeof obj[prop] === 'object') {
            if (obj[prop] !== null) {
                rootPath = path;
                if (!Array.isArray(obj)) {
                    path ? path += `.${prop}` : path += `${prop}`;
                } 
                //result = (getProp(obj[prop], path, keyWords, fields) || result)
                getProp(obj[prop], path, keyWords, fields);
                path = rootPath;
            }
        } else {
            path ? itemPath = `${path}.${prop}` : itemPath = `${prop}`;
            if (fields.includes(itemPath) && typeof obj[prop] === 'string') {
                for (let keyWord in keyWords) {
                    if (obj[prop].toLowerCase().split(' ').includes(keyWords[keyWord])) {
                        // result = (result || true);
                        keyWordsSet.delete(keyWords[keyWord]);
                    }
                }

            }
        }
    }
    // return result
}

function filterCollection(arr, keyWords, isSearchAllKeys, ...fields) {

    keyWords = keyWords.toLowerCase().split(' ');


    let resultArr = arr.filter(function(obj){

        keyWordsSet = new Set(keyWords);

        getProp(obj, '', keyWords, fields);
     
        if ((isSearchAllKeys && keyWordsSet.size === 0) || 
            (!isSearchAllKeys && keyWordsSet.size < keyWords.length)) {
            return true
        } else return false;
    });
    console.log(resultArr);
};

filterCollection(vehicles, 'ua_ua ru_ru test', true, 'name', 'description', 'contentType.name', 'locales.uniq', 'locales.name', 'locales.description');


