$(document).ready(function () {
    $('.tabs-title').click(function () {
        $('.tabs-title.active').toggleClass('active');
        $(this).toggleClass('active');
        $('.tabs-content li.active').toggleClass('active');
        $($('.tabs-content li').get($('.tabs-title').index(this))).toggleClass('active');
    })
});
