let userInput;
let numCount = 0;

while (!(+userInput % 1 === 0) || isNaN(parseInt(userInput))) {
//Варианты проверки на целое число:
// !(+userInput % 1 === 0)
// !(+userInput ^ 0 === +userInput)
// !Number.isInteger(+userInput)
    userInput = prompt('Введите целое (не отрицательное) число:', '');
};

for (let i = 0; i <= userInput; i++) {
    if ((i % 5 === 0) && (i !== 0)) {
        console.log(i)
        numCount++;
    }
}

if (numCount === 0) console.log('Sorry, no numbers');

//Дополнительное задание - вывести простые числа от m до n
let userInputM, userInputN, validateM, validateN;

while (!validateM || !validateN) {
        let strAlert = 'Ошибка!';
        userInputM = prompt('Введите целое число m:', '');
        userInputN = prompt('Введите целое число n:', '');
        validateM = (+userInputM % 1 === 0) && !isNaN(parseInt(userInputM));
        validateN = (+userInputN % 1 === 0) && !isNaN(parseInt(userInputN));
        if (!validateM || !validateN) {
            strAlert += validateM ? '' : ' Число m = ' + userInputM + ' некорректно!!!';
            strAlert += validateN ? '' : ' Число n = ' + userInputN + ' некорректно!!!';
            alert(strAlert);
        }
};

if (+userInputM < +userInputN) {
    let strResultPrimeNumbers = '';
    for (let curRangeNum = +userInputM; curRangeNum <= +userInputN; curRangeNum++) {
        // curRangeNum - текущее число из диапазона от m до n для проверки на то, что оно простое
        if (curRangeNum > 1) {
            let factorNumCount = 0; // Кол-во множителей, на которые число из диапазона делится без остатка
            for (let factorNum = 2; factorNum < curRangeNum; factorNum++) {
                // factorNum - множитель для проверки делимости
                if (curRangeNum % factorNum === 0) factorNumCount++;
            };
            if (factorNumCount === 0) strResultPrimeNumbers += ' ' + curRangeNum + ',';
        }
    }
    if (strResultPrimeNumbers.length > 0) strResultPrimeNumbers = strResultPrimeNumbers.substring(0, strResultPrimeNumbers.length - 1);
    console.log('Простые числа в диапазоне от ' + userInputM + ' до ' + userInputN + ':' + strResultPrimeNumbers)
} else {
    alert ('Ошибка! Число m = ' + userInputM + ' должно быть больше числа n = ' + userInputN);
}
