const rows = 30;
const cols = 30;

let table = document.createElement('table');
table.classList.add('table');
table.innerHTML = `
        <tbody></tbody>
`;
document.body.append(table);

table.addEventListener('click', (e) => {
    if (e.target.tagName === 'TD') e.target.classList.toggle('clicked')
});


for (let i = 1; i <= rows; i++) {
    let tr = document.createElement('tr');
    table.append(tr);
    for (let j = 1; j <= cols; j++) {
        let td = document.createElement('td');
        td.classList.add('td');
        tr.append(td);
    }
};

document.body.addEventListener('click', (e) => {
    console.log(e.target.tagName);
    if (e.target.tagName !== 'TD' &&
        e.target.tagName !== 'TR' &&
        e.target.tagName !== 'TBODY' &&
        e.target.tagName !== 'TABLE') {
            table.classList.toggle('invBackColor')
        }
});
