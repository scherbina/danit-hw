
let input = document.body.querySelector(".input");
let label = document.body.querySelector(".label");

let spanCorrect = document.createElement('span');
spanCorrect.classList.add('note', 'noteCorrect');
spanCorrect.innerText = `Текущая цена: `;
label.before(spanCorrect);

let spanCorrectClose = document.createElement('span');
spanCorrectClose.classList.add('noteCorrectClose');
spanCorrectClose.innerText = `x`;
spanCorrect.append(spanCorrectClose);

let spanInCorrect = document.createElement('span');
spanInCorrect.innerText = 'Please enter correct price';
spanInCorrect.classList.add('note', 'noteInCorrect');
label.after(spanInCorrect);

input.addEventListener('change', showNote);
input.addEventListener('blur', showNote);
spanCorrectClose.addEventListener('click', hideNote);

function showNote(e) {
    if (e.type === 'keydown' && e.key !== 'Enter') return
    if (!input.value) {
        input.classList.remove('inputCorrect');
        input.classList.remove('inputInCorrect');
        spanCorrect.style.display = '';
        spanInCorrect.style.display = '';
        return
    }

    let inputCoords = getCoords(input);
    
    if (!input.value || isNaN(input.value) || +input.value < 0) {
        spanInCorrect.style.display = 'inline-block';
        spanCorrect.style.display = '';
        input.classList.remove('inputCorrect')
        input.classList.add('inputInCorrect')
        spanInCorrect.style.left = inputCoords.left + "px";
        spanInCorrect.style.top = inputCoords.top + input.offsetHeight + "px";
    } else {
        spanCorrect.firstChild.data = `Текущая цена: ${input.value}`;
        spanCorrect.style.display = 'flex';
        spanInCorrect.style.display = '';
        input.classList.remove('inputInCorrect')
        input.classList.add('inputCorrect')
        spanCorrect.style.left = inputCoords.left + "px";
        spanCorrect.style.top = inputCoords.top - spanCorrect.offsetHeight - 1 + "px";
    }
}

function hideNote() {
    spanCorrect.style.display = '';
    input.value = '';
}

function getCoords(elem) {
    let box = elem.getBoundingClientRect();
    return {
      top: box.top + pageYOffset,
      left: box.left + pageXOffset
    };
}
