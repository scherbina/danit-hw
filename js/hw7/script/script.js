const arrSimple = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', 10101];
const arr1 = ['hello', '', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', null];
const arr2 = ['1', '2', '3', 'sea', 'user', 23, true, {name: 'Mikhail', lname: 'Scherbina', contact: {phone: '+381234567', email: 'email@exmp.com'}}];

//Функция для основоного задания
function arrayToListInnerHTML(obj) {
    obj = obj.map(item => `<li>${item}</li>`);
    let ul = document.createElement('ul');
    for (item in obj) {
        let li = document.createElement('li');
        ul.append(li);
        li.innerHTML = obj[item];
    };
    document.body.append(ul);
}

//Функция для дополнительного задания
function arrayToListNode(obj) {
    let ul = document.createElement('ul');
    for (item in obj) {
        let li = document.createElement('li');
        ul.append(li);
        if (typeof obj[item] === 'object' && obj[item] !== null) {
            Array.isArray(obj) ? li.innerHTML = 'ARRAY:' : li.innerHTML = `${item}:`;
            li.append(arrayToListNode(obj[item]))
        } else  {
            obj[item] ? li.innerHTML = `<li>${obj[item]}</li>` : `<li></li>`;
            
        }
    };
    return ul;
}

//Пример простого задания
arrayToListInnerHTML(arrSimple);

//Пример дополнительного задания
document.body.append(arrayToListNode(['TEST', arr1, arr2]));

//Обратный отсчет
let pCountDown = document.createElement('p');
pCountDown.style.cssText = 'position: fixed; top: 50%; left: 50%; transform: translateX(-50%) translateY(-50%); font-size: 100px; font-weight: bold;';
document.body.append(pCountDown);

let i=10;
pCountDown.innerHTML = i;

let timerID = setTimeout(function changeElementText(el) {
    if (--i < 0) {
        document.body.innerHTML = '';
        return
    };
    el.innerText = i;
    let timerID = setTimeout(changeElementText, 1000, el)
},1000,pCountDown);
