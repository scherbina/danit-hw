let inNum = '';

function factorial(n) {
    if (n === 1) return 1;
    return n * factorial(n - 1);
};

do {
    inNum = prompt('Enter number: ', inNum || '');
} while (inNum === null || inNum.trim() === '' || isNaN(+inNum) || +inNum % 1 !== 0 || +inNum < 1);

console.log (`Factorial(${inNum}) = ${factorial(parseInt(inNum))}`);
