let student = {
    name: null,
    'last name': null,
    table: {}
}

let inFName, inLName;

do {
    inFName = prompt('Введите имя: ', inFName || '');
    if (inFName === null) continue;
    inFName = inFName.trim();
} while (inFName === null || inFName === '' || inFName.search(/\d|\s/g) !== -1);    
student.name = inFName;

do {
    inLName = prompt('Введите фамилию: ', inLName || '');
    if (inLName === null) continue;
    inLName = inLName.trim();
} while (inLName === null || inLName === '' || inLName.search(/\d|\s/g) !== -1);    
student['last name'] = inLName;

let course, score;
do {
    course = prompt('Введите название предмета: ', '');
    if (course === null) break;
    course = course.trim();
    if (course !== '') {
        do {
            score = prompt('Введите оценку за предмет: ', '');
            if (score === null) continue;
            score = score.trim();
        } while (score === '' && isNaN(+score) && +score % 1 !== 0)
        student.table[course] = +score;
    }
} while (true);    

let sumScore = badScore = numScore = 0;
for (key in student.table) {
    sumScore += student.table[key];
    numScore++;
    if (student.table[key] < 4) badScore++;
}

const avgScore = sumScore / numScore;
console.log(`Средний бал: ${avgScore}`);
!badScore ? console.log(`Студент ${student.name} ${student['last name']} переведен на следующий курс`) : console.log(`Студент ${student.name} ${student['last name']} отчислен по неуспеваимости`);
if (avgScore > 7 && !badScore) console.log('Студенту назначена стипендия');
