function createNewUser() {
    let inFName = inLName = '';
    let newUser = {
        getLogin: function () {
            if (!!this.firstName && !!this.lastName) {
                return (this.firstName[0]+this.lastName).toLowerCase()
            } else {
                return 'ERROR'
            }
        },
        getFullName: function() {
            return (`${this.firstName} ${this.lastName}`)
        },
        setFirstName: function (newFName) {
            Object.defineProperty(this, 'firstName', {writable: true, configurable: true});
            this.firstName = newFName;
            Object.defineProperty(this, 'firstName', {writable: false, configurable: true});
        },
        setLastName: function (newLName) {
            Object.defineProperty(this, 'lastName', {writable: true, configurable: true});
            this.lastName = newLName;
            Object.defineProperty(this, 'lastName', {writable: false, configurable: true});
        }
    };

    do {
        inFName = prompt('Enter first name: ', inFName || '');
        if (inFName === null) continue;
        inFName = inFName.trim();
    } while (inFName === null || inFName === '' || inFName.search(/\d|\s/g) !== -1);    
    
    do {
        inLName = prompt('Enter first name: ', inLName || '');
        if (inLName === null) continue;
        inLName = inLName.trim();
    } while (inLName === null || inLName === '' || inLName.search(/\d|\s/g) !== -1);    
    
    newUser.setFirstName(inFName);
    newUser.setLastName(inLName);

    return newUser;
}

let newUser = createNewUser();

newUser.firstName = 'Попытка прямого изменения имени';
newUser.lastName = 'Попытка прямого изменения фамилии';

console.log(`Full name: ${newUser.getFullName()}`);
console.log(`Login: ${newUser.getLogin()}`);

