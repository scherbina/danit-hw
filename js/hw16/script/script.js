let inNum = '';
let inF0 = '';
let inF1 = '';

function fibonachi(n, f0, f1) {
    if (n < 0) {
        return fibonachi(n + 2, f0, f1) - fibonachi(n + 1, f0, f1);
    } else {
        if (n === 0) return f0;
        if (n === 1) return f1;
        return fibonachi(n - 1, f0, f1) + fibonachi(n - 2, f0, f1);
    }
};

do {
    inF0 = prompt('Enter number F0: ', inNum || '');
} while (inF0 === null || inF0.trim() === '' || isNaN(+inF0) || +inF0 % 1 !== 0);

do {
    inF1 = prompt('Enter number F1: ', inNum || '');
} while (inF1 === null || inF1.trim() === '' || isNaN(+inF1) || +inF1 % 1 !== 0);

do {
    inNum = prompt('Enter number Fibonachi (N): ', inNum || '');
} while (inNum === null || inNum.trim() === '' || isNaN(+inNum) || +inNum % 1 !== 0);

console.log (`F${inNum} = ${fibonachi(parseInt(inNum), parseInt(inF0), parseInt(inF1))}`);
