$(document).ready(function(){

    $('.clientFeedbackDetailList').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
    });

    $('.clientFeedbackSlider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.clientFeedbackDetailList',
        arrows: true,
        focusOnSelect: true,
        centerMode: true,
        centerPadding: 50,
        prevArrow: '<i class="fas fa-chevron-left sliderPrevArrow"></i>',
        nextArrow: '<i class="fas fa-chevron-right sliderNextArrow"></i>',
    });

    let $grid = $('.grid').masonry({
            itemSelector: '.gridItem:not(.hide)',
            gutter: 20,
            fitWidth: true,
            transitionDuration: '0.8s',
    });

    $('.secBestImagesBtn').click(function() {
        const $btn = $(this);
        renderSpinner($btn);

        setTimeout(() => {
            $('.spinner').remove();
            $('.gridLoadMore.hide').removeClass('hide');
            $btn.hide();
            $grid.masonry( 'appended', $('.gridLoadMore'))
        }, 2000)
    });

    $('.tabServicesTitle').click(function () {
        $('.tabServicesTitle.active').toggleClass('active');
        $(this).toggleClass('active');
        $('.tabsServicesContentWrapper.active').toggleClass('active');
        $($('.tabsServicesContentWrapper').get($('.tabServicesTitle').index(this))).toggleClass('active');
    });

    $('.tabWorkFilterTitle').click(function() {
        const $tabWorkFilterTitle = $(this);
        $('.tabWorkFilterTitle.active').removeClass('active');
        $tabWorkFilterTitle.addClass('active');
        renderWorkGalery();
    });

    $('.workGalleryItem').hover(
        function () {
            const $workGalleryItem = $(this);
            const workType = $workGalleryItem.data('worktype');
            
            $workGalleryItem
                .append(`
                <div class="workGalleryItemDesc">
                    <p class="workGalleryItemDescBtns">
                        <button class="workGalleryItemDescBtn btnLink"><i class="fa fa-link workGallery " aria-hidden="true"></i></button>
                        <button class="workGalleryItemDescBtn btnSearch"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </p>
                    <span class="workGalleryItemDescTitle">creative design</span>
                    <span class="workGalleryItemDescType"></span>
                </div>
            `)
                .find('.workGalleryItemDescType')
                .html(workType);
            $('.workGalleryItemDesc').hide().slideDown('fast')

        }, function () {
            $('.workGalleryItemDesc')
                .slideUp('fast', function () {
                    const $workGalleryItemDesc = $(this);
                    $workGalleryItemDesc.remove()
                })
        });

    $('.secWorksBtn').click(function() {
        const $btn = $(this);
        renderSpinner($btn);

        setTimeout(() => {
            $('.spinner').remove();
            if ($('.loadMoreSecond.hide').length > 0) {
                $('.loadMoreSecond.hide').removeClass('hide');
            } else {
                $('.loadMoreThird.hide').removeClass('hide');
                $btn.hide();
            }
            renderWorkGalery();
        }, 2000)
    });

    function renderWorkGalery () {
        const $tabWorkFilterTitle = $('.tabWorkFilterTitle.active');
        const workType = $tabWorkFilterTitle.data('worktype');

        if (workType !== 'all') {
            $('.workGalleryItem').hide();
            $(`.workGalleryItem[data-workType="${workType}"]:not(.workGalleryItem.hide)`).show();
            $('.secWorksBtn').hide();
        } else {
            $('.workGalleryItem:not(.workGalleryItem.hide)').show();
            if ($('.loadMoreThird.hide').length > 0) {
                $('.secWorksBtn').show();
            }
        }
    }

    $('.gridItem:not(.groupItem), .gridInnerItem').hover(
        function () {
            const $gridItem = $(this);
            
            $gridItem
                .append(`
                <div class="gridItemHover">
                    <div class="gridItemBtns">
                        <button class="gridItemBtn gridItemBtnSearch"><i class="fa fa-search" aria-hidden="true"></i></button>
                        <button class="gridItemBtn gridItemBtnExpand"><i class="fa fa-expand-arrows-alt" aria-hidden="true"></i></button>
                    </div>
                </div>
            `);
            $('.gridItemHover').hide().fadeIn();

        }, function () {
            $('.gridItemHover')
                .fadeOut(function () {
                    const $gridItemHover = $(this);
                    $gridItemHover.remove()
                })
    });

    function renderSpinner ($el) {
        const elCoord = $el.offset();
        const elWidth = $el.outerWidth();

        $el
            .after('<i class="fas fa-spinner fa-spin"></i>')
            .next()
            .addClass('spinner')
            .offset({top: elCoord.top - 55, left: elCoord.left + elWidth/2 - 25});
    }

    $('.benefit').click(function () {
        alert(`Click on benefits: ${$(this).find('p').text()}`)
    });

});